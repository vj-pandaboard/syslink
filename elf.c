/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#include "elf.h"
#include "log.h"

#include <gelf.h> /* gelf_getehdr */
#include <string.h> /* strcmp */
#include <fcntl.h> /* O_RDONLY */
#include <unistd.h> /* close */

static inline bool
get_phdr(Elf *e,
	 FILE *f,
	 segment_func func,
	 void *cont)
{
	GElf_Phdr phdr;
	size_t phnum;

	if (elf_getphdrnum (e, &phnum) != 0)
		return false;

	for (size_t i = 0; i < phnum; i++) {
		if (gelf_getphdr(e, i, &phdr) == NULL)
			return false;

		if (phdr.p_type != PT_LOAD)
			continue;

		if (func)
			func(f,
			     phdr.p_vaddr,
			     phdr.p_memsz,
			     phdr.p_filesz,
			     phdr.p_offset,
			     cont);
	}

	return true;
}

static inline bool
get_symbol(Elf *e, const char *symbol, uint32_t *value)
{
	Elf_Scn *scn = NULL;
	GElf_Sym sym;
	GElf_Shdr shdr;
	Elf_Data *data = NULL;

	while ((scn = elf_nextscn(e, scn)) != NULL) {
		if (gelf_getshdr(scn, &shdr) == NULL)
			return false;

		if (shdr.sh_type != SHT_SYMTAB)
			continue;

		if ((data = elf_getdata(scn, data)) == NULL)
			return false;

		int scount = shdr.sh_size / shdr.sh_entsize;
		for (int s = 0; s < scount; s++) {
			if (gelf_getsym(data, s, &sym) == NULL)
				return false;

			uint32_t pointer = sym.st_value;
			if (pointer == 0)
				continue;

			uint8_t type = ELF32_ST_TYPE(sym.st_info);
			if (type != STT_OBJECT && type != STT_FUNC)
				continue;

			const char *name =
				elf_strptr(e, shdr.sh_link, sym.st_name);
			if (!strcmp(symbol, name)) {
				*value = pointer;
				return true;
			}
		}
	}

	return false;
}

static inline bool
get_entry(Elf *e, uint32_t *entry)
{
	GElf_Ehdr ehdr;

	if (gelf_getehdr(e, &ehdr) == NULL)
		return false;

	*entry = ehdr.e_entry;

	return true;
}

bool
get_elf_info(const char *filename,
	     uint32_t *entry,
	     uint32_t *start,
	     segment_func func,
	     void *cont)
{
	int fd;
	Elf *e;
	bool ret = false;

	if (elf_version(EV_CURRENT) == EV_NONE)
		return false;

	if ((fd = open(filename, O_RDONLY, 0)) < 0)
		return false;

	if ((e = elf_begin(fd, ELF_C_READ, NULL)) == NULL)
		goto bail;

	if (elf_kind(e) != ELF_K_ELF)
		goto bail;

	if (!get_entry(e, entry))
		goto bail;

	if (!get_symbol(e, "_Ipc_ResetVector", start))
		goto bail;

	{
		FILE *f;
		if ((f = fdopen(fd, "r")) == NULL)
			goto bail;

		if (!get_phdr(e, f, func, cont))
			goto bail;
	}

	ret = true;

bail:
	if (!ret)
		pr_err("elf error!: %s", elf_errmsg(-1));

	elf_end(e);
	close(fd);

	return ret;
}
