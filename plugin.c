/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#include "plugin.h"

#include "gstsyslinkvdec.h"

GstDebugCategory *gstsyslink_debug;

static gboolean
plugin_init(GstPlugin *plugin)
{
	gstsyslink_debug = _gst_debug_category_new("syslink", 0, "SysLink stuff");

	if (!gst_element_register(plugin,
				  "syslinkvdec",
				  GST_RANK_PRIMARY,
				  GST_SYSLINK_VDEC_TYPE))
		return FALSE;

	return TRUE;
}

GstPluginDesc gst_plugin_desc = {
	.major_version = 0,
	.minor_version = 10,
	.name = "syslink",
	.description = (gchar *) "Texas Instruments SysLink elements",
	.plugin_init = plugin_init,
	.version = VERSION,
	.license = "LGPL",
	.source = "gst-syslink",
	.package = "none",
	.origin = "none",
};
