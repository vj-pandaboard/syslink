/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#include "syslink.h"
#include "dce.h"
#include "tiler.h"
#include "log.h"
#include "util.h"
#include "list.h"

#include <unistd.h> /* close */
#include <string.h> /* strerror */
#include <errno.h>

int32_t width, height, stride;
FILE *instrm, *outstrm;

static inline bool
get_regions(int ipc_handle,
	    int proc_handle,
	    uint16_t *nentries,
	    struct sharedregion_region **regions)
{
	struct sharedregion_config sr_cfg;

	if (!s_sharedregion_get_config(ipc_handle, &sr_cfg))
		return false;

	*nentries = sr_cfg.num_entries;

	*regions = calloc(*nentries,
			  sizeof(struct sharedregion_region));

	if (!s_sharedregion_get_region_info(ipc_handle,
					    *regions))
		goto bail;

	if (!s_procmgr_map_sharedregions(proc_handle,
					 *nentries,
					 *regions))
		goto unmap_and_bail;

	return true;

unmap_and_bail:
	s_procmgr_unmap_sharedregions(sr_cfg.num_entries,
				      *regions);

bail:
	free(regions);
	return false;
}

static inline size_t
write_buf(FILE *strm,
	  int8_t *buf,
	  int width,
	  int height,
	  int stride)
{
	int i, len;
	int8_t *p;
	size_t size, w;

	size = 0;
	for (i = 0; i < height; i++) {
		p = buf;
		len = width;

		while (true) {
			if (len > 0) {
				w = fwrite(p, 1, len, strm);
				if (w > 0) {
					size += w;
					p += w;
					len -= w;
				} else
					break;
			} else
				break;
		}

		buf += stride;
	}

	return size;
}

static size_t
write_stream(FILE *strm,
	     int8_t *y,
	     int8_t *uv)
{
	size_t size;

	size = write_buf(strm, y, width, height, stride);
	size += write_buf(strm, uv, width, height / 2, stride);

	return size;
}

#include "viddec.h"

struct buffer_list {
	void *iova;
	uintptr_t pa, pa_uv;
	struct list_head list;
};

struct buffer_list in_bl, out_bl;

static inline void
bufferlist_push(struct buffer_list *head,
		struct buffer_list *item)
{
	list_add_tail(&item->list,
		      &head->list);
}

static struct buffer_list *
bufferlist_pop(struct buffer_list *head)
{
	struct buffer_list *b;

	b = list_entry(head->list.next, struct buffer_list, list);
	if (&b->list != &head->list)
		list_del(&b->list);

	return b;
}

static bool
decode(struct codec_engine *ce,
       void *codec,
       void *ib,
       void *ob,
       void *ia,
       void *oa)
{
	struct buffer_list *bin, *bout;
	size_t r;
	int i;
	bool ret = false;

	struct buf_desc *inbufs = (struct buf_desc *) ib;
	struct buf_desc *outbufs = (struct buf_desc *) ob;
	struct viddec_in_args *inargs = (struct viddec_in_args *) ia;
	struct viddec_out_args *outargs = (struct viddec_out_args *) oa;

	bin = bufferlist_pop(&in_bl);

	while(true) {
		r = fread(bin->iova,
			  1,
			  width * height,
			  instrm);

		if (r <= 0) {
			ret = true;
			break;
		}

		inargs->num_bytes = r;
		inbufs->descs[0].buf = (int8_t *) bin->pa;
		inbufs->descs[0].buf_size.bytes = r;

		bout = bufferlist_pop(&out_bl);

		if (!bout)
			break;

		inargs->input_id = (uintptr_t) bout;

		outbufs->descs[0].buf = (int8_t *) bout->pa;
		outbufs->descs[1].buf = (int8_t *) bout->pa_uv;

		if (!dce_viddec_process(ce,
					codec,
					ib,
					ob,
					ia,
					oa)) {
			pr_err("extended error: %08x", outargs->extended_error);
			break;
		}

		for (i = 0; outargs->output_id[i] != 0; i++) {
			struct rect *r = &outargs->display_bufs.buf_desc[0].active_frame_region;
			int yoff = r->top_left.y * stride + r->top_left.x;
			int uvoff = r->top_left.y * stride / 2 + r->top_left.x;

			bout = (struct buffer_list *) outargs->output_id[i];
			write_stream(outstrm,
				     bout->iova + yoff,
				     bout->iova + uvoff + stride * (height + 4 * 24));
		}

		for (i = 0; outargs->free_buf_id[i] != 0; i++) {
			bout = (struct buffer_list *) outargs->free_buf_id[i];
			bufferlist_push(&out_bl, bout);
		}
	}

	return ret;
}

static inline void
alloc_tile(struct codec_engine *ce,
	   uint16_t width,
	   uint16_t height,
	   void **va,
	   uintptr_t *pa)
{
	if (!dce_tile_alloc(ce, width, height, va))
		return;

	*pa = dce_tile_get_pa(ce, *va);
}

static bool
config_buffers(struct codec_engine *ce,
	       void **inbuf,
	       void **outbuf)
{
	bool ret = false;

	{
		INIT_LIST_HEAD(&in_bl.list);
		struct buffer_list *item = calloc(1, sizeof(struct buffer_list));
		alloc_tile(ce, width, height, &item->iova, &item->pa);
		bufferlist_push(&in_bl, item);
	}

	struct single_buf_desc_2 in_bufs[] = {
		{ .mem_type = 0, },
	};

	if (!viddec_set_buffer_desc(ce, 1, in_bufs, inbuf))
		goto bail;

	int32_t tw = ALIGN(width + 2 * 32, 7);
	int32_t th = height + 4 * 24;

	struct single_buf_desc_2 *out_bufs;
	if (stride == 4096) { /* 2D */
		struct single_buf_desc_2 bd[] = {
			{
				.mem_type = 1,
				.buf_size.tile_mem = {
					.width = tw,
					.height = th,
				},
			},
			{
				.mem_type = 2,
				.buf_size.tile_mem = {
					.width = tw,
					.height = th / 2,
				},
			},
		};
		out_bufs = bd;
	} else { /* 1D */
		struct single_buf_desc_2 bd[] = {
			{
				.mem_type = 4,
				.buf_size.bytes = stride * th,
			},
			{
				.mem_type = 4,
				.buf_size.bytes = stride * th / 2,
			},
		};

		th = 0;
		tw = stride * th + stride * th / 2;

		out_bufs = bd;
	}

	if (!viddec_set_buffer_desc(ce, 2, out_bufs, outbuf))
		goto bail;

	{
		int i, nbuf;

		INIT_LIST_HEAD(&out_bl.list);

		nbuf = MIN(16, 32768 / ((width / 16) * (height / 16))) + 3;
		for (i = 0; i < nbuf; i++) {
			struct buffer_list *item;

			item = calloc(1, sizeof(struct buffer_list));
			alloc_tile(ce, tw, th, &item->iova, &item->pa);

			/* set uv physical address */
			void *iova = item->iova + th * stride;
			tiler_get_pa(ce->tiler_handle, iova, &item->pa_uv);

			bufferlist_push(&out_bl, item);
		}
	}

	ret = true;

bail:
	return ret;
}

static bool
config_params(struct codec_engine *ce)
{
	bool ret = false;
	void *params, *codec;
	void *dynparams;
	void *status;
	void *inbuf, *outbuf;
	void *inargs, *outargs;

	codec = NULL;
	dynparams = NULL;
	status = NULL;
	inbuf = outbuf = NULL;
	inargs = outargs = NULL;

	if (!h264vdec_set_params(ce,
				 height,
				 width,
				 &params))
		return false;

	if (!dce_viddec_create(ce,
			       "ivahd_h264dec",
			       params,
			       &codec))
		goto bail;

	if (!codec)
		goto bail;

	if (!h264vdec_set_dynparams(ce,
				    &dynparams))
		goto bail;

	if (!h264vdec_set_status(ce,
				 &status))
		goto bail;

	if (!dce_viddec_control(ce,
				codec,
				1, /* set params */
				dynparams,
				status))
		goto bail;

	/* not entirely sure why we need to call this here.
	 * just copying OMX (DCE). */
	if (!dce_viddec_control(ce,
				codec,
				5, /* query buffers properties */
				dynparams,
				status))
		goto bail;

	if (!config_buffers(ce,
			    &inbuf,
			    &outbuf))
		goto bail;

	if (!h264vdec_set_in_args(ce, &inargs))
		goto bail;

	if (!h264vdec_set_out_args(ce, &outargs))
		goto bail;

	if (!decode(ce,
		    codec,
		    inbuf,
		    outbuf,
		    inargs,
		    outargs))
		goto bail;

	ret = true;

bail:
	pr_debug("status error = %d", viddec_status_get_error(status));

	if (inargs)
		dce_buffer_free(ce, inargs);

	if (outargs)
		dce_buffer_free(ce, outargs);

	if (inbuf)
		dce_buffer_free(ce, inbuf);

	if (outbuf)
		dce_buffer_free(ce, outbuf);

	if (codec)
		dce_viddec_delete(ce, codec);

	if (status)
		dce_buffer_free(ce, status);

	if (dynparams)
		dce_buffer_free(ce, dynparams);

	dce_buffer_free(ce, params);

	return ret;
}

static bool
run(struct rcm_client *rcm)
{
	bool ret = false;

	int tiler_handle = tiler_open();
	if (tiler_handle == -1)
		return false;

	struct codec_engine ngix = {
		.rcm = rcm,
		.tiler_handle = tiler_handle,
	};

	if (!dce_init(&ngix))
		goto deinit_engine;

	if (!dce_open(&ngix, "ivahd_vidsvr"))
		goto deinit_engine;

	ret = config_params(&ngix);

	dce_close(&ngix);

deinit_engine:
	dce_deinit(&ngix);
	return ret;
}

int
main(int argc, char **argv)
{
	int ret = EXIT_FAILURE;
	uint16_t nentries;
	struct sharedregion_region *regions = NULL;

	width = ALIGN(720, 4);
	height = ALIGN(480, 4);
	stride = 4096;

	instrm = fopen("/home/user/vss.264", "r");
	if (!instrm) {
		pr_err("could not open input file");
		return false;
	}

	outstrm = fopen("/dev/null", "w");
	if (!outstrm) {
		pr_err("could not open output file");
		return false;
	}

	int ipc_handle = s_ipc_open();
	if (ipc_handle == -1)
		return ret;

	int proc_handle = s_proc_open();
	if (proc_handle == -1)
		goto bail;

	if (!s_ipc_setup(ipc_handle))
		goto bail;

	if (!get_regions(ipc_handle,
			 proc_handle,
			 &nentries,
			 &regions))
		goto destroy_and_bail;

	bool res = false;
	{
		struct rcm_client rcm = {
			.ipc_handle = ipc_handle,
			.sr_nentries = nentries,
			.regions = regions,
		};

		res = run(&rcm);
	}

	if (res)
		ret = EXIT_SUCCESS;

	if (!s_procmgr_unmap_sharedregions(nentries, regions))
		pr_err("unmapping shared regions failed!: %s", strerror(errno));

	free(regions);

destroy_and_bail:
	if (!s_ipc_destroy(ipc_handle))
		pr_err("ipc destroy failed!: %s", strerror(errno));

bail:
	if (proc_handle != -1)
		close(proc_handle);

	close(ipc_handle);

	fclose(instrm);
	fclose(outstrm);

	return ret;
}
