/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#ifndef PLUGIN_H
#define PLUGIN_H

#include <gst/gst.h>

G_BEGIN_DECLS

extern GstDebugCategory *gstsyslink_debug;

G_END_DECLS

#endif /* PLUGIN_H */
