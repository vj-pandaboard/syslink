/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

/* Direct Codec Engine */

#ifndef DCE_H
#define DCE_H

#include "rcm.h"

struct codec_engine {
	struct rcm_client *rcm;
	int tiler_handle;
	void *priv;
};

struct codec_engine *dce_new(int ipc_handle);
void dce_free(struct codec_engine *ce);

bool dce_init(struct codec_engine *ce);
bool dce_deinit(struct codec_engine *ce);

bool dce_open(struct codec_engine *ce,
	      const char *name);
bool dce_close(struct codec_engine *ce);

bool dce_buffer_alloc(struct codec_engine *ce,
		      size_t size,
		      void **ptr);
bool dce_buffer_free(struct codec_engine *ce,
		     void *ptr);

bool dce_tile_alloc(struct codec_engine *ce,
		    int16_t width,
		    int16_t height,
		    void **ptr);
void *dce_tile_get_iova(struct codec_engine *ce,
			uint32_t pa);
uintptr_t dce_tile_get_pa(struct codec_engine *ce,
			  void *iova);

bool dce_viddec_create(struct codec_engine *ce,
		       const char *name,
		       void *params,
		       void **codec);
bool dce_viddec_delete(struct codec_engine *ce,
		       void *codec);
bool dce_viddec_control(struct codec_engine *ce,
			void *codec,
			unsigned int id,
			void *params,
			void *status);
bool dce_viddec_process(struct codec_engine *ce,
			void *codec,
			void *inbufs,
			void *outbufs,
			void *inargs,
			void *outargs);

#endif /* DCE_H */
