/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

/* Remote Command Messaging */

#include "rcm.h"
#include "syslink.h"
#include "log.h"

#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stddef.h> /* for offsetof */
#include <unistd.h> /* for close */

struct msg_list {
	struct rcm_client_msg *msg;
	struct list_head list;
};

static inline struct sharedregion_region*
get_regions(int ipc_handle,
	    int proc_handle,
	    uint16_t *num)
{
	struct sharedregion_config cfg;
	struct sharedregion_region *regions;

	if (!s_sharedregion_get_config(ipc_handle,
				       &cfg))
		return NULL;

	*num = cfg.num_entries;

	regions = calloc(*num, sizeof(*regions));

	if (!s_sharedregion_get_region_info(ipc_handle,
					    regions))
		goto bail;

	if (!s_procmgr_map_sharedregions(proc_handle,
					 *num,
					 regions))
		goto unmap_and_bail;

	return regions;

unmap_and_bail:
	s_procmgr_unmap_sharedregions(*num,
				      regions);

bail:
	free(regions);
	return NULL;
}

struct rcm_client *
rcm_client_new(int ipc_handle)
{
	int proc_handle = s_proc_open();
	if (proc_handle == -1)
		return NULL;

	uint16_t num;
	struct sharedregion_region *regions;

	regions = get_regions(ipc_handle,
			      proc_handle,
			      &num);

	close(proc_handle);

	if (!regions)
		return NULL;

	struct rcm_client *self;

	self = calloc(1, sizeof(*self));
	self->ipc_handle = ipc_handle;
	self->sr_nentries = num;
	self->regions = regions;

	return self;
}

void
rcm_client_free(struct rcm_client *rcm)
{
	s_procmgr_unmap_sharedregions(rcm->sr_nentries,
				      rcm->regions);
	free(rcm->regions);
	free(rcm);
}

static inline bool
create_message_queue(int handle,
		     void **queue,
		     uint32_t *qid)
{
	void *sync = NULL;

	if (!s_messageq_get_sync(handle,
				 &sync))
		return false;

	if (!s_messageq_create(handle,
			       NULL,
			       &sync,
			       queue,
			       qid))
		return false;

	return true;
}

bool
rcm_client_init(struct rcm_client *self,
		char *name)
{
	INIT_LIST_HEAD(&self->mailbox);

	/* create the queue for inbound messages */
	if (!create_message_queue(self->ipc_handle,
				  &self->msg_queue,
				  &self->msg_queue_id))
		goto bail;

	/* create the queue for error messages */
	if (!create_message_queue(self->ipc_handle,
				  &self->err_queue,
				  &self->err_queue_id))
		goto bail;

	if (!s_messageq_open(self->ipc_handle,
			     name,
			     &self->queue_id))
		goto bail;

	return true;

bail:
	rcm_client_deinit(self);
	return false;
}

bool
rcm_client_deinit(struct rcm_client *self)
{
	bool ret = true;

	struct msg_list *entry, *tmp;
	list_for_each_entry_safe(entry, tmp, &self->mailbox, list) {
		list_del(&entry->list);
		free(entry);
	}

	if (self->queue_id != 0xffff) {
		ret &= s_messageq_close(self->ipc_handle,
					self->queue_id);
		if (ret)
			self->queue_id = 0xffff;

	}

	if (self->msg_queue) {
		ret &= s_messageq_delete(self->ipc_handle,
					 self->msg_queue);
		if (ret)
			self->msg_queue = NULL;
	}

	if (self->err_queue) {
		ret &= s_messageq_delete(self->ipc_handle,
					 self->err_queue);
		if (ret)
			self->err_queue = NULL;
	}

	return ret;
}

static pthread_mutex_t atomic_lock = PTHREAD_MUTEX_INITIALIZER;

static inline sig_atomic_t
generate_msg_id()
{
	static int id = 0xffff;

	pthread_mutex_lock(&atomic_lock);
	if (id == 0xffff)
		id = 1;
	else
		++id;
	pthread_mutex_unlock(&atomic_lock);

	return id;
}

struct rcm_client_packet {
	struct messageq_msg queue_msg;
	uint16_t desc;
	uint16_t id;
	struct rcm_client_msg msg;
};

bool
rcm_client_alloc(struct rcm_client *self,
		 uint32_t size,
		 struct rcm_client_msg **msg)
{
	if (size < sizeof(uint32_t))
		size = sizeof(uint32_t);

	/* Total memory size needed for headers and payload */
	size_t t = sizeof(struct rcm_client_packet) - sizeof(uint32_t) + size;

	uint32_t sr_addr;
	if (!s_messageq_alloc(self->ipc_handle,
			      1, /* heap_id = 1 (DOMX/DCE heap) */
			      t,
			      &sr_addr))
		return false;

	if (sr_addr == (uint32_t) ~0) /* is it invalid? */
		return false;

	uintptr_t pa;
	if (!s_sharedregion_get_pa(sr_addr,
				   self->sr_nentries,
				   self->regions,
				   &pa))
		return false;

	struct rcm_client_packet *pkt;
	pkt = (struct rcm_client_packet *) pa;

	pkt->desc = 0;
	pkt->id = generate_msg_id();
	pkt->msg.pool_id = 0x8000;
	pkt->msg.job_id = 0;
	pkt->msg.func_idx = 0xffffffff;
	pkt->msg.res = 0;
	pkt->msg.size = size;

	*msg = &pkt->msg;

	return true;
}

static inline bool
get_sharedregion_addr(struct rcm_client *self,
		      uint32_t pa,
		      uint32_t *sr_addr)
{
	uint16_t sr_id;
	if (!s_sharedregion_get_id(pa,
				   self->sr_nentries,
				   self->regions,
				   &sr_id))
		return false;

	if (!s_sharedregion_get_sr_addr(pa,
					sr_id,
					self->sr_nentries,
					self->regions,
					sr_addr))
		return false;

	return true;
}

static inline struct rcm_client_packet *
get_rcm_client_packet(struct rcm_client_msg *msg)
{
	uintptr_t ret = ((uintptr_t) msg) - offsetof(struct rcm_client_packet, msg);
	return (struct rcm_client_packet *) ret;
}

static inline struct rcm_client_packet *
get_rcm_client_packet_msgq(struct messageq_msg *queue_msg)
{
	uintptr_t ret = ((uintptr_t) queue_msg) \
		- offsetof(struct rcm_client_packet, queue_msg);
	return (struct rcm_client_packet *) ret;
}


bool
rcm_client_free_msg(struct rcm_client *self,
		    struct rcm_client_msg *msg)
{
	uintptr_t pa = (uintptr_t) get_rcm_client_packet(msg);

	uint32_t sr_addr;
	if (!get_sharedregion_addr(self, pa, &sr_addr))
		return false;

	return s_messageq_free(self->ipc_handle, sr_addr);
}

static inline struct rcm_client_msg *
get_message_from_mailbox(struct rcm_client *self,
			 uint16_t id)
{
	struct msg_list *entry, *tmp;
	struct rcm_client_msg *msg;
	struct rcm_client_packet *packet;

	list_for_each_entry_safe(entry, tmp, &self->mailbox, list) {
		msg = entry->msg;
		packet = get_rcm_client_packet(msg);
		if (id == packet->id) {
			list_del(&entry->list);
			free(entry);
			return msg;
		}
	}
	return NULL;
}

static inline bool
get_messageq(struct rcm_client *self,
	     uint32_t timeout,
	     struct messageq_msg **msg)
{
	uint32_t sr_addr;
	int ret = s_messageq_get(self->ipc_handle,
				 self->msg_queue,
				 timeout,
				 &sr_addr);
	if (ret < 0 && ret != -ETIME && ret != -20)
		return false;
	else if (ret == -ETIME) {
		*msg = NULL;
		return true;
	}

	if (sr_addr == (uint32_t) ~0) /* is it invalid? */
		return false;

	uintptr_t pa;
	if (!s_sharedregion_get_pa(sr_addr,
				   self->sr_nentries,
				   self->regions,
				   &pa))
		return false;

	*msg = (struct messageq_msg *) pa;

	return true;
}

static inline struct rcm_client_msg *
get_message_from_messageq(struct rcm_client *self,
			  uint16_t id)
{
	struct messageq_msg *msgq = NULL;

	while (true) {
		if (!msgq)
			if (!get_messageq(self, 0, &msgq))
				return NULL;

		while (msgq) {
			struct rcm_client_packet *packet;
			packet = get_rcm_client_packet_msgq(msgq);

			if (id == packet->id) {
				return &packet->msg;
				/* TODO: recipient thingy */
			} else {
				/* TODO: recipient thingy */
				/* not our message, so mailbox it */
				struct msg_list *tmp;
				tmp = malloc(sizeof(struct msg_list));
				tmp->msg = &packet->msg;
				list_add(&tmp->list, &self->mailbox);
			}

			if (!get_messageq(self, 0, &msgq))
				return NULL;
		}

		/* let's wait for ever for a message */
		if (!get_messageq(self, ~0, &msgq))
			return NULL;
	}

	/* unreachable */
	return NULL;
}

static inline bool
get_return_msg(struct rcm_client *self,
	       const uint16_t id,
	       struct rcm_client_msg **msg)
{
	*msg = get_message_from_mailbox(self, id);
	if (*msg)
		return true;

	*msg = get_message_from_messageq(self, id);
	if (*msg)
		return true;

	return false;
}

static inline bool
rpc_sync(struct rcm_client *self,
	 uint16_t cmd,
	 struct rcm_client_msg *in,
	 struct rcm_client_msg **out)
{
	struct rcm_client_packet *packet;
	packet = get_rcm_client_packet(in);

	packet->desc |= cmd;
	uint16_t msg_id = packet->id;

	struct messageq_msg *msgq;
	msgq = &packet->queue_msg;
	s_messageq_set_reply_queue(self->msg_queue_id, msgq);

	uintptr_t pa = (uintptr_t) packet;
	uint32_t sr_addr;
	if (!get_sharedregion_addr(self,
				   pa,
				   &sr_addr))
		return false;

	if (!s_messageq_put(self->ipc_handle,
			    self->queue_id,
			    sr_addr))
		return false;

	if (!get_return_msg(self, msg_id, out))
		return false;

	packet = get_rcm_client_packet(*out);
	int status = (0x0F00 & packet->desc) >> 8;

	return status == 0;
}

bool
rcm_client_get_symbol_index(struct rcm_client *self,
			    const char *name,
			    uint32_t *index)
{
	bool ret = false;
	size_t len = strlen(name) + 1;
	struct rcm_client_msg *msg;

	*index = -5; /* invalid function index */

	if (!rcm_client_alloc(self, len, &msg))
		return false;

	msg->size = len;
	memcpy(msg->data, name, len);

	if (!rpc_sync(self,
		      0x400, /* query symbol index */
		      msg,
		      &msg))
		goto bail;

	ret = true;
	*index = msg->data[0];

bail:
	rcm_client_free_msg(self, msg);
	return ret;
}

bool
rcm_client_exec(struct rcm_client *self,
		struct rcm_client_msg *in,
		struct rcm_client_msg **out)
{
	if (!rpc_sync(self,
		      0x100, /* client exec message */
		      in,
		      out))
		return false;

	return true;
}
