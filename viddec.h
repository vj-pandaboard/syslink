/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#ifndef VIDDEC_H
#define VIDDEC_H

#include "dce.h"

/**
 * Union describing a buffer size
 *
 *  More advanced memory types (e.g. tiled memory) cannot
 *  indicate buffer size using a simple integer, but rather
 *  must indicate memory size using a width and height.
 */
union buf_size {
	struct {
		int32_t width;  /**< Width of @c buf in 8-bit bytes. */
		int32_t height;  /**< Height of @c buf in 8-bit bytes. */
	} tile_mem;
	int32_t bytes;      /**< Size of @c buf in 8-bit bytes. */
};

/**
 *  @brief      Single buffer descriptor
 *
 *  @remarks    This buffer descriptor contains a @c memType field, and uses
 *              the XDM_BufSize union to indicate the size of the buffer.
 *              As a result, this data type can be used to describe
 *              complex memory types (e.g. tiled memory).
 */
struct single_buf_desc_2 {
	int8_t   *buf;       /**< Pointer to a buffer address. */
	int16_t  mem_type;    /**< Memory type.
			       *
			       *   @sa XDM_MemoryType
			       */
	int16_t usage_mode;   /**< Memory usage descriptor.
			       *
			       *   @remarks   This field is set by the owner
			       *              of the buffer (typically the
			       *              application), and read by users
			       *              of the buffer (including the
			       *              algorithm).
			       *
			       *   @sa XDM_MemoryUsageMode
			       */
	union buf_size buf_size;   /**< Buffer size(for tile memory/row memory */
	int32_t  access_mask; /**< Mask filled by the algorithm, declaring
			       *   how the buffer was accessed <b>by the
			       *   algorithm processor</b>.
			       *
			       *   @remarks  If the buffer was <b>not</b>
			       *             accessed by the algorithm
			       *             processor (e.g., it was filled
			       *             via DMA or other hardware
			       *             accelerator that <i>doesn't</i>
			       *             write through the algorithm's
			       *             CPU), then no bits in this mask
			       *             should be set.
			       *
			       *   @remarks  It is acceptible (and
			       *             appropriate!)to set several
			       *             bits in this mask if the
			       *             algorithm accessed the buffer
			       *             in several ways.
			       *
			       *   @remarks  This mask is often used by the
			       *             application and/or framework
			       *             to appropriately manage cache
			       *             on cache-based systems.
			       *
			       *   @sa XDM_AccessMode
			       */
};

/**
 *  @brief      Buffer descriptor
 *
 *  @remarks    This advanced buffer uses the XDM2_SingleBufDesc, and is
 *              typically used for codecs which must reflect <i>types</i>
 *              of memory.  For example, video codecs may need to indicate
 *              whether tiled memory is used.
 */
struct buf_desc {
	int32_t   num_bufs;   /**< Number of buffers in @c descs
			       * array.
			       *
			       *   @remarks  Must be less than
			       *             #XDM_MAX_IO_BUFFERS.
			       */
	struct single_buf_desc_2 descs[16]; /** Array of buffer
					     * descriptors.
					     */
};

/**
 *  @brief      Defines the input arguments for all IVIDDEC3 instance
 *              process function.
 *
 *  @extensibleStruct
 *
 *  @sa         IVIDDEC3_Fxns::process()
 */
struct viddec_in_args {
	int32_t size;            /**< @sizeField */
	int32_t num_bytes;        /**< Size of input data in bytes, provided
				   *   to the algorithm for decoding.
				   */
	int32_t input_id;         /**< The decoder will attach
				   *   this ID with the corresponding output
				   *   frames.
				   *
				   *   @remarks   This is useful when frames
				   *   require re-ordering (e.g. B frames).
				   *
				   *   @remarks   When there is no re-ordering,
				   *   IVIDDEC3_OutArgs#outputID will be same
				   *   as this inputID field.
				   *
				   *   @remarks   Zero (0) is not a supported
				   *              inputID.  This value is
				   *              reserved for cases when there
				   *              is no output buffer provided in
				   *              IVIDDEC3_OutArgs::displayBufs.
				   *
				   *   @sa IVIDDEC3_OutArgs::outputID.
				   */
};

/**
 *  @brief      2-dimensional point
 */
struct point {
    int32_t x;
    int32_t y;
};

/**
 *  @brief      Rectangle
 */
struct rect {
    struct point top_left;
    struct point bottom_right;
};

/**
 *  @brief      Detailed buffer descriptor for video buffers.
 */
struct video_buf_desc {
	int32_t num_planes;       /**< Number of video planes.
				   *
				   *   @remarks   This must be in the range
				   *              0 - #IVIDEO_MAX_NUM_PLANES.
				   *
				   *   @todo      Need further description.
				   */
	int32_t num_meta_planes;   /**< Number of meta data planes.
				    *
				    *   @remarks   This must be in the range
				    *          0 - #IVIDEO_MAX_NUM_METADATA_PLANES.
				    *
				    *   @todo      Need further description.
				    */
	int32_t data_layout;      /**< Field interleaved, field separated.
				   *
				   *   @todo      Need further description.
				   *              Is there an enum we should
				   *              reference for valid values?
				   *              Perhaps IVIDEO_VideoLayout?
				   */
	struct single_buf_desc_2 plane_desc[3]; /**< Picture buffers. */
	struct single_buf_desc_2 metadata_plane_desc[3]; /**< Meta planes.
							  *
							  *   @remarks   For MB Info & alpha blending
							  *
							  *   @todo      Need further description.
							  */
	int32_t  second_field_offset_width[3];  /**< Offset
						 *   for second field (width in pixels).
						 *
						 *   @remarks   Valid only if above pointer
						 *              is not NULL.
						 *
						 *   @todo      Need further description.
						 *              Which "above pointer"?  Is
						 *              this relavent to planeDesc
						 *              or metadataPlaneDesc... or
						 *              both?  Is the "width in pixels"
						 *              comment correct?
						 */
	int32_t  second_field_offset_height[3];  /**< Offset
						  *   for second field (height in lines).
						  *
						  *   @remarks   Valid only if above pointer
						  *              is not NULL.
						  *
						  *   @todo      Need further description.
						  *              Which "above pointer"?  Is
						  *              this relavent to planeDesc
						  *              or metadataPlaneDesc... or
						  *              both?
						  */
	int32_t  image_pitch[3];  /**< Image pitch for
				   *   each plane.
				   */
	struct rect    image_region;    /**< Image region (top left and bottom
				      *   right).
				      */
	struct rect    active_frame_region; /**< Active frame region (top left and
					  *   bottom right).
					  */
	int32_t  extended_error;  /**< @extendedErrorField
				   *
				   *   @remarks   This field is not required
				   *              for encoders.
				   */
	int32_t frame_type;       /**< @copydoc IVIDEO_FrameType
				   *
				   *   @remarks   This field is not required
				   *              for encoder input buffer.
				   *
				   *   @sa IVIDEO_FrameType
				   */
	int32_t top_field_first_flag;/**< Flag to indicate when the application
				      *   should display the top field first.
				      *
				      *   @remarks   Valid values are XDAS_TRUE
				      *              and XDAS_FALSE.
				      *
				      *   @remarks   This field is only app licable
				      *              for interlaced content, not
				      *              progressive.
				      *
				      *   @remarks   This field does not apply to
				      *              encoder recon bufs.
				      */
	int32_t repeat_first_field_flag;/**< Flag to indicate when the first field
					 *   should be repeated.
					 *
					 *   @remarks   Valid values are XDAS_TRUE
					 *              and XDAS_FALSE.
					 *
					 *   @remarks   This field is only applicable
					 *              for interlaced content, not
					 *              progressive.
					 *
					 *   @remarks   This field does not apply to
					 *              encoder recon bufs.
					 */   /* not required for encoder input buffer */
	int32_t frame_status;     /**< @copydoc  IVIDEO_OutputFrameStatus
				   *
				   *   @sa IVIDEO_OutputFrameStatus
				   *
				   *   @remarks   This field does not apply to
				   *              encoder recon bufs.
				   */  /* not required for encoder input buffer */
	int32_t repeat_frame;     /**< Number of times the display process
				   *   needs to repeat the displayed progressive
				   *   frame.
				   *
				   *   @remarks   This information is useful
				   *              for progressive
				   *              content when the
				   *              decoder expects the
				   *              display process to
				   *              repeat the displayed
				   *              frame for a certain
				   *              number of times. This
				   *              is useful for pulldown
				   *              (frame/field
				   *              repetition by display
				   *              system) support
				   *              where the display
				   *              frame rate is
				   *              increased without
				   *              increasing the decode
				   *              frame rate.
				   *
				   *   @remarks   The default value is 0.
				   *
				   *   @remarks   This field does not apply to
				   *              encoder recon bufs.
				   */    /* not required for encoder input buffer */
	int32_t content_type;     /**< Content type of the buffer.
				   *
				   *   @remarks This is useful when
				   *              the content is both
				   *              interlaced and
				   *              progressive.  The
				   *              display process can
				   *              use this field to
				   *              determine how to
				   *              render the display
				   *              buffer.
	                           *
	                           *   @sa        IVIDEO_ContentType
	                           */
	int32_t chroma_format;     /**< @copydoc XDM_ChromaFormat
				    *
				    *   @sa XDM_ColorFormat
				    */
	int32_t scaling_width;    /**< Scaled image width for post processing.
				   *   @remarks   This field is not required
				   *              for encoders.
				   *
				   *   @todo      Is this in pixels?
				   *
				   *   @todo      Should this field and
				   *              scalingHeight use a XDM_Rect
				   *              data type?
				   */
	int32_t scaling_height;   /**< Scaled image width for post processing.
				   *
				   *   @remarks   This field is not required
				   *              for encoders.
				   *
				   *   @todo      Is this in pixels?
				   */
	int32_t range_mapping_luma;/**< Range Mapping Luma
				    *
				    *   @todo      Need further description.
				    *
				    *   @todo      We should explore what we did
				    *              in the speech interfaces and
				    *              perhaps create an ivideo_vc1.h
				    *              with VC1-specific definitions.
				    */
	int32_t range_mapping_chroma;/**< Range Mapping Chroma
				      *
				      *   @todo      Need further description.
				      *
				      *   @todo      We should explore what we did
				      *              in the speech interfaces and
				      *              perhaps create an ivideo_vc1.h
				      *              with VC1-specific definitions.
				      */
	int32_t enable_range_reduction_flag;/**< Flag indicating whether or not
					     *   to enable range reduction.
					     *
					     *   @remarks   Valid values are XDAS_TRUE
					     *              and XDAS_FALSE.
					     *
					     *   @todo      We should explore what we did
					     *              in the speech interfaces and
					     *              perhaps create an ivideo_vc1.h
					     *              with VC1-specific definitions.
					     */
};

/**
 *  @brief      Defines the run time output arguments for
 *              all IVIDDEC3 instance objects.
 *
 *  @extensibleStruct
 *
 *  @remarks    The size of this struct may vary when
 *              IVIDDEC3_OutArgs.displayBufsMode is set to
 *              #IVIDDEC3_DISPLAYBUFS_EMBEDDED (see details in
 *              IVIDDEC3_OutArgs.displayBufs.bufDesc).
 *
 *  @remarks    When IVIDDEC3_OutArgs.displayBufsMode is set to
 *              #IVIDDEC3_DISPLAYBUFS_EMBEDDED, the number of elements in the
 *              IVIDDEC3_OutArgs.displayBufs.bufDesc array is a constant (and
 *              can be acquired by calling IVIDDEC3_Fxns.control() and looking
 *              in the IVIDDEC3_Status.maxOutArgsDisplayBufs field.  Note that
 *              any extended fields follow the
 *              IVIDDEC3_OutArgs.displayBufs.bufDesc array.
 *
 *  @sa         IVIDDEC3_Fxns.process()
 */
struct viddec_out_args {
	int32_t size;       /**< @sizeField
			     *
			     *   @remarks   Extra care must be taken when
			     *              setting this field as the
			     *              size of even the base data
			     *              can vary because of the
			     *              #IVIDDEC3_OutArgs.displayBufs
			     *              field.
			     *
			     *   @sa IVIDDEC3_OutArgs.displayBufs
			     */
	int32_t extended_error;   /**< @extendedErrorField */
	int32_t bytes_consumed;   /**< Number of bytes consumed. */
	int32_t output_id[20]; /**< Output ID corresponding
                                 *   to @c displayBufs[].
                                 *
                                 *   @remarks   A value of zero (0) indicates
                                 *              an invalid ID.  The first zero
                                 *              entry in array will indicate
                                 *              end of valid outputIDs within
                                 *              the array.  Hence the
                                 *              application can stop reading the
                                 *              array when it encounters the
                                 *              first zero entry.
                                 *
                                 *   @sa IVIDDEC3_OutArgs.displayBufs
                                 *   @sa IVIDDEC3_InArgs.inputID
                                 */
	struct video_buf_desc decoded_bufs; /**< The decoder fills this structure with
                                 *   buffer pointers to the decoded frame.
                                 *   Related information fields for the
                                 *   decoded frame are also populated.
                                 *
                                 *   When frame decoding is not complete, as
                                 *   indicated by
                                 *   IVIDDEC3_OutArgs.outBufsInUseFlag,
                                 *   the frame data in this structure will be
                                 *   incomplete.  However, the algorithm will
                                 *   provide incomplete decoded frame data
                                 *   in case application wants to use
                                 *   it for error recovery purposes.
                                 *
                                 *   @sa IVIDDEC3_OutArgs.outBufsInUseFlag
                                 */
	int32_t free_buf_id[20]; /**< This is an
                                 *   array of inputID's corresponding to the
                                 *   buffers that have been unlocked in the
                                 *   current process call.
                                 *
                                 *   @remarks   Buffers returned to the
                                 *              application for display (via
                                 *              IVIDDEC3_OutArgs.displayBufs)
                                 *              continue to be owned by the
                                 *              algorithm until they are
                                 *              released - indicated by
                                 *              the ID being returned in this
                                 *              @c freeBuf array.
                                 *
                                 *   @remarks   The buffers released by the
                                 *              algorithm are indicated by
                                 *              their non-zero ID (previously
                                 *              provided via
                                 *              IVIDDEC3_InArgs.inputID).
                                 *
                                 *   @remarks   A value of zero (0) indicates
                                 *              an invalid ID.  The first zero
                                 *              entry in array will indicate
                                 *              end of valid freeBufIDs within
                                 *              the array.  Hence the
                                 *              application can stop searching
                                 *              the array when it encounters the
                                 *              first zero entry.
                                 *
                                 *   @remarks   If no buffer was unlocked in
                                 *              the process call,
                                 *              @c freeBufID[0] will
                                 *              have a value of zero.
                                 *
                                 *   @sa IVIDDEC3_InArgs.inputID
                                 *   @sa IVIDDEC3_OutArgs.displayBufs
                                 */
	int32_t out_bufs_in_use_flag; /**< Flag to indicate that the @c outBufs
                                 *   provided with the IVIDDEC3_Fxns.process()
                                 *   call are in use.  No @c outBufs are
                                 *   required to be supplied with the next
                                 *   IVIDDEC3_Fxns.process() call.
                                 *
                                 *   @remarks   Valid values are #XDAS_TRUE
                                 *              and #XDAS_FALSE.
                                 */
	int32_t display_bufs_mode; /**< Indicates which mode the
                                 *   #IVIDDEC3_OutArgs.displayBufs are
                                 *   presented in.
                                 *
                                 *   @remarks   See the
                                 *              IVIDDEC3_DisplayBufsMode enum
                                 *              for the values this field may
                                 *              contain.
                                 *
                                 *   @remarks   This will be set to the same
                                 *              value the application provided
                                 *              at creation time via
                                 *              #IVIDDEC3_Params.displayBufsMode.
                                 *
                                 *   @sa IVIDDEC3_Params.displayBufsMode
                                 *   @sa IVIDDEC3_DisplayBufsMode
                                 */
	union {
		struct video_buf_desc buf_desc[1];/**< Array containing display frames
                                 *   corresponding to valid ID entries
                                 *   in the @c outputID[] array.
                                 *
                                 *   @remarks   The number of elements in this
                                 *              array is not necessarily 1, and
                                 *              should be acquired by the app
                                 *              via the
                                 *              IVIDDEC3_Status.maxNumDisplayBufs
                                 *              field - acquired by calling
                                 *              IVIDDEC3_Fxns.control() with
                                 *              #XDM_GETSTATUS.
                                 *              The application should acquire
                                 *              this prior to calling
                                 *              IVIDDEC3_Fxns.process().
                                 *
                                 *   @remarks   Because of the variable size
                                 *              of this array, care must be
                                 *              taken when setting the
                                 *              IVIDDEC3_OutArgs.size field
                                 *              to include the complete size of
                                 *              this struct.
                                 *
                                 *   @remarks   Entries in the array
                                 *              corresponding to invalid
                                 *              ID values (zero) in
                                 *              IVIDDEC3_OutArgs.outputID[] will
                                 *              set zero value for the following
                                 *              fields in the IVIDEO2_BufDesc
                                 *              structure:  @c numPlanes,
                                 *              @c numMetaPlanes.
                                 *
                                 *   @remarks   Implied by the previous remark,
                                 *              as this array corresponds to
                                 *              buffer IDs indicated by
                                 *              @c outputID[], elements of
                                 *              this array are undefined if
                                 *              the corresponding @c outputID[]
                                 *              element is zero (0).
                                 *
                                 */
		struct video_buf_desc *pbuf_desc[20]; /**< Array containing
                                 *   pointers to display frames corresponding
                                 *   to valid ID entries in the @c outputID[]
                                 *   array.
                                 *
                                 *   @remarks   These buffers must be allocated
                                 *              by the application, and provided
                                 *              <i>into</i> this "outArgs"
                                 *              structure by the app.
                                 */
	} display_bufs;              /**< Display Buffers union.
                                 *
                                 *   @remarks   This field is complex.  The
                                 *              value in
                                 *              #IVIDDEC3_OutArgs.displayBufsMode
                                 *              indicates how the user should
                                 *              interact with this union field.
                                 *              If #IVIDDEC3_OutArgs.displayBufsMode
                                 *              is
                                 *              #IVIDDEC3_DISPLAYBUFS_EMBEDDED,
                                 *              this field should be referenced
                                 *              via the
                                 *              IVIDDEC3_OutArgs.bufDesc[] array
                                 *              who's number of elements is
                                 *              determined via the
                                 *              #IVIDDEC3_OutArgs.outputID[]
                                 *              array.  If this field is
                                 *              #IVIDDEC3_DISPLAYBUFS_PTRS,
                                 *              this field should be referenced
                                 *              via the
                                 *              IVIDDEC3_OutArgs.pBufDesc[]
                                 *              array.
                                 *
                                 *   @sa IVIDDEC3_OutArgs.bufDesc
                                 *   @sa IVIDDEC3_OutArgs.pBufDesc
                                 */
};

bool viddec_set_params(struct codec_engine *ce,
		       uint32_t height,
		       uint32_t width,
		       int32_t size,
		       void **data);
bool viddec_set_dynparams(struct codec_engine *ce,
			  int32_t size,
			  void **data);
bool viddec_set_status(struct codec_engine *ce,
		       int32_t size,
		       void **data);
int32_t viddec_status_get_error(void *data);
bool viddec_set_in_args(struct codec_engine *ce,
			int32_t size,
			void **data);
bool viddec_set_out_args(struct codec_engine *ce,
			 int32_t size,
			 void **data);
bool viddec_set_buffer_desc(struct codec_engine *ce,
			    int32_t num,
			    struct single_buf_desc_2 bufs[],
			    void **buffer);

bool h264vdec_set_status(struct codec_engine *ce,
			 void **data);
bool h264vdec_set_params(struct codec_engine *ce,
			 int32_t height,
			 int32_t width,
			 void **data);
bool h264vdec_set_dynparams(struct codec_engine *ce,
			    void **data);
bool h264vdec_set_in_args(struct codec_engine *ce,
			  void **data);
bool h264vdec_set_out_args(struct codec_engine *ce,
			   void **data);

#endif /* VIDDEC_H */
