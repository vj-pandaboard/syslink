/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#include "syslink.h"
#include "util.h"
#include "log.h"

#include <fcntl.h> /* open */
#include <unistd.h> /* close */
#include <sys/ioctl.h> /* ioctl */
#include <sys/mman.h> /* mmap */
#include <string.h> /* memcpy */
#include <errno.h>

#include <signal.h> /* sigfillset */
#include <stdio.h> /* perror */

/* ioctl driver identifier */
#define SL 0xE0

/* ioctl module masks */
#define MULTIPROC    2
#define SHAREDREGION 50
#define GATEMP       70
#define MESSAGEQ     110
#define NOTIFY       170
#define HEAPBUFMP    30
#define HEAPMEMMP    150
#define LISTMP       90
#define IPC          130
#define NAMESERVER   10
#define PROCMGR      0x100
#define RPROC        0x50
#define DMM          0x56
#define DEH          0x45

/* Used to calculate the ioctl per syslink module */
#define S_IOC(module, num)	((module) + (num))

#define SHAREDREGION_GETCONFIG _IOWR(SL, S_IOC(SHAREDREGION, 0), struct sharedregion_args)
#define SHAREDREGION_GETHEAP   _IOWR(SL, S_IOC(SHAREDREGION, 7), struct sharedregion_get_heap_args)
#define SHAREDREGION_GETREGIONSINFO _IOWR(SL, S_IOC(SHAREDREGION, 12), struct sharedregion_get_info_args)
#define MESSAGEQ_GETSYNC       _IOWR(SL, S_IOC(MESSAGEQ, 3), struct messageq_get_sync_args)
#define MESSAGEQ_CREATE        _IOWR(SL, S_IOC(MESSAGEQ, 4), struct messageq_create_args)
#define MESSAGEQ_DELETE        _IOWR(SL, S_IOC(MESSAGEQ, 5), struct messageq_delete_args)
#define MESSAGEQ_OPEN          _IOWR(SL, S_IOC(MESSAGEQ, 6), struct messageq_open_args)
#define MESSAGEQ_CLOSE         _IOWR(SL, S_IOC(MESSAGEQ, 7), struct messageq_close_args)
#define MESSAGEQ_ALLOC         _IOWR(SL, S_IOC(MESSAGEQ, 9), struct messageq_alloc_args)
#define MESSAGEQ_FREE          _IOWR(SL, S_IOC(MESSAGEQ, 10), struct messageq_free_args)
#define MESSAGEQ_PUT           _IOWR(SL, S_IOC(MESSAGEQ, 11), struct messageq_put_args)
#define MESSAGEQ_REGISTERHEAP  _IOWR(SL, S_IOC(MESSAGEQ, 12), struct messageq_register_heap_args)
#define MESSAGEQ_UNREGISTERHEAP _IOWR(SL, S_IOC(MESSAGEQ, 13), struct messageq_unregister_heap_args)
#define MESSAGEQ_GET           _IOWR(SL, S_IOC(MESSAGEQ, 16), struct messageq_get_args)
#define NOTIFY_THREADATTACH    _IOWR(SL, S_IOC(NOTIFY, 12), int)
#define NOTIFY_THREADDETACH    _IOWR(SL, S_IOC(NOTIFY, 13), int)
#define HEAPBUFMP_PARAMSINIT   _IOWR(SL, S_IOC(HEAPBUFMP, 3), struct heapbufmp_args)
#define HEAPBUFMP_CREATE       _IOWR(SL, S_IOC(HEAPBUFMP, 4), struct heapbufmp_create_args)
#define HEAPBUFMP_DELETE        _IOWR(SL, S_IOC(HEAPBUFMP, 5), struct heapbufmp_delete_args)
#define HEAPBUFMP_SHAREDMEMREQ _IOWR(SL, S_IOC(HEAPBUFMP, 11), struct heapbufmp_gsmrs_args)
#define HEAPMEMMP_ALLOC        _IOWR(SL, S_IOC(HEAPMEMMP, 9), struct heapmemmp_alloc_args)
#define HEAPMEMMP_FREE          _IOWR(SL, S_IOC(HEAPMEMMP, 10), struct heapmemmp_free_args)

#define PROCMGR_OPEN            S_IOC(PROCMGR, 7)
#define PROCMGR_CLOSE           S_IOC(PROCMGR, 8)
#define PROCMGR_GETATTACHPARAMS S_IOC(PROCMGR, 9)
#define PROCMGR_ATTACH          S_IOC(PROCMGR, 10)
#define PROCMGR_DETACH          S_IOC(PROCMGR, 11)
#define PROCMGR_GETSTATE        S_IOC(PROCMGR, 17)

#define RPROC_START             _IOWR(RPROC, 1, struct rproc_start_args)
#define RPROC_STOP              _IOWR(RPROC, 2, int)
#define RPROC_IOCREGEVENT       _IOR(RPROC, 4, struct rproc_regevent_args)
#define RPROC_IOCUNREGEVENT     _IOR(RPROC, 5, struct rproc_regevent_args)

#define IPC_SETUP              _IOWR(SL, S_IOC(IPC, 0), struct ipc_args)
#define IPC_DESTROY            _IOWR(SL, S_IOC(IPC, 1), struct ipc_args)
#define IPC_CONTROL            _IOWR(SL, S_IOC(IPC, 2), struct ipc_args)

#define DMM_IOCSETTLBENT       _IO(DMM, 0)
#define DMM_IOCMEMMAP          _IO(DMM, 1)
#define DMM_IOCCREATEPOOL      _IO(DMM, 6)
#define DMM_IOCDELETEPOOL      _IO(DMM, 7)
#define IOMMU_REGEVENT         _IO(DMM, 10)
#define IOMMU_UNREGEVENT       _IO(DMM, 11)

#define DEH_REGEVENT           _IOW(DEH, 1, struct deh_args)
#define DEH_UNREGEVENT         _IOW(DEH, 2, struct deh_args)

/* page size */
#define SZ_4K  0x1000
#define SZ_64K 0x10000
#define SZ_1M  0x100000
#define SZ_16M 0x1000000

int
s_ipc_open()
{
	return open("/dev/syslink_ipc", O_SYNC | O_RDWR);
}

int
s_proc_open()
{
	return open("/dev/syslink-procmgr", O_SYNC | O_RDWR);
}

int
s_proc_tesla_open()
{
	return open("/dev/omap-rproc0", O_SYNC | O_RDWR);
}

int
s_proc_sysm3_open()
{
	return open("/dev/omap-rproc1", O_SYNC | O_RDWR);
}

int
s_proc_appm3_open()
{
	return open("/dev/omap-rproc2", O_SYNC | O_RDWR);
}

int
s_dmm_mpu_open()
{
	return open("/dev/iovmm-omap0", O_SYNC | O_RDWR);
}

int
s_dmm_tesla_open()
{
	return open("/dev/iovmm-omap1", O_SYNC | O_RDWR);
}

int
s_deh_tesla_open()
{
	return open("/dev/omap-devh0", O_SYNC | O_RDWR);
}

int
s_deh_sysm3_open()
{
	return open("/dev/omap-devh1", O_SYNC | O_RDWR);
}

int
s_deh_appm3_open()
{
	return open("/dev/omap-devh2", O_SYNC | O_RDWR);
}

struct sharedregion_args { /* 32 bytes */
	void *cfg;
	int32_t data[6];
	int32_t status;
};

bool
s_sharedregion_get_config(int handle, struct sharedregion_config *cfg)
{
	struct sharedregion_args args = {
		.cfg = cfg,
	};
	if (ioctl(handle, SHAREDREGION_GETCONFIG, &args) < 0)
		return false;

	return args.status == 0;
}

struct sharedregion_get_info_args { /* 32 bytes */
	struct sharedregion_region *regions;
	int32_t data[6];
	int32_t status;
};

bool
s_sharedregion_get_region_info(int handle,
			       struct sharedregion_region *regions)
{
	struct sharedregion_get_info_args args = {
		.regions = regions,
	};
	if (ioctl(handle, SHAREDREGION_GETREGIONSINFO, &args) < 0)
		return false;

	return args.status == 0;
}

struct sharedregion_get_heap_args { /* 32 bytes */
	uint16_t id;
	void *heap;
	int32_t data[5];
	int32_t status;
};

bool
s_sharedregion_get_heap(int handle,
			uint16_t id,
			void **heap)
{
	struct sharedregion_get_heap_args args = {
		.id = id,
	};

	if (ioctl(handle, SHAREDREGION_GETHEAP, &args) < 0)
		return false;

	*heap = args.heap;

	return args.status == 0;
}

static uint32_t
get_number_offset_bits(uint32_t num_entries)
{
	uint32_t index = 0;

	if (num_entries == 0)
		index = 0;
	else if (num_entries == 1)
		index = 1;
	else {
		num_entries -= 1;
		while(num_entries) {
			index++;
			num_entries = num_entries >> 1;
		}
	}

	return 0x20 - index;
}

/*
 * Returns the address pointer associated with the shared region
 * pointer.
 */
bool
s_sharedregion_get_pa(uint32_t sr_addr,
		      uint32_t num_entries,
		      struct sharedregion_region *regions,
		      uintptr_t *pa)
{
	*pa = (uint32_t) ~0; /* invalid by default */

	uint32_t offset = get_number_offset_bits(num_entries);
	uint32_t mask = (1 << offset) - 1;
	uint32_t i = sr_addr >> offset;
	if (i >= num_entries)
		return false;

	struct sharedregion_entry *e;
	e = &regions[i].entry;
	*pa = (sr_addr & mask) + e->base;

	return true;
}

bool
s_sharedregion_get_id(uint32_t pa,
		      uint32_t num_entries,
		      struct sharedregion_region *regions,
		      uint16_t *id)
{
	uint16_t i;

	*id = 0xffff; /* invalid id */
	for (i = 0; i < num_entries; i++) {
		struct sharedregion_entry *e;
		e = &regions[i].entry;
		uint32_t b = e->base;

		if (e->is_valid && pa >= b && pa < b + e->len) {
			*id = i;
			return true;
		}
	}

	return false;
}

/*
 * Returns the shared region pointer associated with the physical
 * address.
 */
bool
s_sharedregion_get_sr_addr(uint32_t pa,
			   uint16_t id,
			   uint32_t num_entries,
			   struct sharedregion_region *regions,
			   uint32_t *sr_addr)
{
	if (id > num_entries)
		return false;

	*sr_addr = (uint32_t) ~0; /* invalid by default */

	struct sharedregion_entry *e;
	e = &regions[id].entry;

	uint32_t b = e->base;
	uint32_t l = e->len;

	if (pa >= b && pa < b + l) {
		uint32_t offset = get_number_offset_bits(num_entries);
		*sr_addr = id << offset | (pa - b);
		return true;
	}

	return false;
}

struct messageq_get_sync_args {
	void *handle;
	void **synchronizer;
	int32_t data[3];
	int32_t status;
};

bool
s_messageq_get_sync(int handle, void **sync)
{
	struct messageq_get_sync_args args = {
		.synchronizer = sync,
	};
	if (ioctl(handle, MESSAGEQ_GETSYNC, &args) < 0)
		return false;

	return args.status == 0;
}

struct messageq_register_heap_args {
	void *heap;
	uint16_t id;
	int16_t data[7];
	int32_t status;
};

bool
s_messageq_register_heap(int handle, uint16_t id, void *heap)
{
	struct messageq_register_heap_args args = {
		.heap = heap,
		.id = id,
	};
	if (ioctl(handle, MESSAGEQ_REGISTERHEAP, &args) < 0)
		return false;

	return args.status == 0;
}

struct messageq_unregister_heap_args {
	uint16_t id;
	int16_t data[9];
	int32_t status;
};

bool
s_messageq_unregister_heap(int handle, uint16_t id)
{
	struct messageq_unregister_heap_args args = {
		.id = id,
	};
	if (ioctl(handle, MESSAGEQ_UNREGISTERHEAP, &args) < 0)
		return false;

	return args.status == 0;
}

struct messageq_create_args {
	void *queue;
	char *name;
	void **sync;
	uint32_t namelen;
	uint32_t qid;
	int32_t status;
};

bool s_messageq_create(int handle,
		       char *name,
		       void **sync,
		       void **queue,
		       uint32_t *id)
{
	uint32_t len = 0;

	if (name)
		len = strlen(name) + 1;

	struct messageq_create_args args = {
		.sync = sync,
		.name = name,
		.namelen = len,
	};

	if (ioctl(handle, MESSAGEQ_CREATE, &args) < 0)
		return false;

	*queue = args.queue;
	*id = args.qid;

	return args.status == 0;
}

struct messageq_delete_args {
	void *queue;
	int32_t data[4];
	int32_t status;
};

bool
s_messageq_delete(int handle, void *queue)
{
	struct messageq_delete_args args = {
		.queue = queue,
	};
	if (ioctl(handle, MESSAGEQ_DELETE, &args) < 0)
		return false;

	return args.status == 0;
}

struct messageq_open_args {
	char *name;
	uint32_t qid;
	uint32_t namelen;
	int32_t data[2];
	int32_t status;
};

bool
s_messageq_open(int handle,
		char *name,
		uint32_t *qid)
{
	uint32_t len = 0;

	if (name)
		len = strlen(name) + 1;

	struct messageq_open_args args = {
		.name = name,
		.namelen = len,
	};

	if (ioctl(handle, MESSAGEQ_OPEN, &args) < 0)
		return false;

	*qid = args.qid;

	return args.status == 0 || args.status == -5;
}

struct messageq_close_args {
	uint32_t qid;
	int32_t data[4];
	int32_t status;
};

bool
s_messageq_close(int handle,
		 uint32_t qid)
{
	struct messageq_close_args args = {
		.qid = qid,
	};

	if (ioctl(handle, MESSAGEQ_CLOSE, &args) < 0)
		return false;

	return args.status == 0;
}

struct messageq_alloc_args {
	uint16_t heap_id;
	uint32_t size;
	uint32_t sr_addr;
	int32_t data[2];
	int32_t status;
};

bool
s_messageq_alloc(int handle,
		 uint16_t heap_id,
		 uint32_t size,
		 uint32_t *sr_addr)
{
	struct messageq_alloc_args args = {
		.heap_id = heap_id,
		.size = size,
	};

	if (ioctl(handle, MESSAGEQ_ALLOC, &args) < 0)
		return false;

	*sr_addr = args.sr_addr;

	return args.status == 0;
}

struct messageq_free_args {
	uint32_t sr_addr;
	int32_t data[4];
	int32_t status;
};

bool
s_messageq_free(int handle,
		uint32_t sr_addr)
{
	struct messageq_free_args args = {
		.sr_addr = sr_addr,
	};

	if (ioctl(handle, MESSAGEQ_FREE, &args) < 0)
		return false;

	return args.status == 0;
}

struct messageq_put_args {
	uint32_t qid;
	uint32_t sr_addr;
	int32_t data[3];
	int32_t status;
};

bool
s_messageq_put(int handle,
	       uint32_t qid,
	       uint32_t sr_addr)
{
	struct messageq_put_args args = {
		.qid = qid,
		.sr_addr = sr_addr,
	};

	if (ioctl(handle, MESSAGEQ_PUT, &args) < 0)
		return false;

	return args.status == 0;
}

struct messageq_get_args {
	void *queue;
	uint32_t timeout;
	uint32_t sr_addr;
	int32_t data[2];
	int32_t status;
};

int
s_messageq_get(int handle,
	       void *queue,
	       uint32_t timeout,
	       uint32_t *sr_addr)
{
	struct messageq_get_args args = {
		.queue = queue,
		.timeout = timeout,
	};

	int status;
	status = ioctl(handle, MESSAGEQ_GET, &args);
	if (status > 0)
		return status;

	*sr_addr = args.sr_addr;

	return args.status;
}

struct event_packet {
	void *next;
	void *prev;
	uint32_t pid;
	uint16_t proc_id;
	uint32_t event_id;
	uint16_t line_id;
	uint32_t data;
	void (*func)(uint16_t, uint16_t, uint32_t, void *, uint32_t);
	void *param;
	bool is_exit;
};

static void *
notify_thread(void *args)
{
	sigset_t set;
	struct event_packet packet;
	int handle = (int) (long) args;

	/* let's block the read operation */
	sigfillset(&set);
	if (pthread_sigmask(SIG_BLOCK, &set, NULL))
		return NULL;

	while (true) {
		memset(&packet, 0, sizeof(packet));
		packet.pid = getpid();
		ssize_t r = read(handle, &packet, sizeof(packet));

		if (r == -1)
			continue;

		if (r == 0)
			return NULL;

		if (r != sizeof(packet))
			continue;

		if (packet.is_exit)
			return NULL;

		if (packet.func)
			packet.func(packet.proc_id,
				    packet.line_id,
				    packet.event_id,
				    packet.param,
				    packet.data);
	}

	return NULL;
}

bool
s_notify_thread_attach(int handle, pthread_t *tid)
{
	int pid = getpid();
	if (ioctl(handle, NOTIFY_THREADATTACH, &pid) < 0)
		return false;

	void *args = (void *) (long) handle;
	if (pthread_create(tid,
			   NULL,
			   notify_thread,
			   args) != 0)
		return false;

	return true;
}

bool
s_notify_thread_detach(int handle, pthread_t tid)
{
	int pid = getpid();
	if (ioctl(handle, NOTIFY_THREADDETACH, &pid) < 0)
		return false;

	if (pthread_join(tid, NULL) != 0)
		return false;

	return true;
}

struct procmgr_open {
	int status;
	uint16_t id;
	void *proc;
	struct procmgr_proc_info info;
};

bool
s_procmgr_open(int handle, uint16_t id, void **ret_proc)
{
	struct procmgr_open args = {
		.id = id,
	};

	if (ioctl(handle, PROCMGR_OPEN, &args) < 0)
		return false;

	*ret_proc = args.proc;
	return args.status == 0;
}

struct procmgr_close {
	int status;
	void *proc;
	struct procmgr_proc_info info;
};

bool
s_procmgr_close(int handle, void *proc)
{
	struct procmgr_close args = {
		.proc = proc,
	};

	if (ioctl(handle, PROCMGR_CLOSE, &args) < 0)
		return false;

	return args.status == 0;
}

struct procmgr_getstate {
	int status;
	void *proc;
	enum procmgr_proc_state state;
};

bool
s_procmgr_get_state(int handle, void *proc, enum procmgr_proc_state *state)
{
	struct procmgr_getstate args = {
		.proc = proc,
	};

	if (ioctl(handle, PROCMGR_GETSTATE, &args) < 0)
		return false;

	*state = args.state;

	return args.status == 0;
}

struct procmgr_get_attach_params {
	int status;
	void *proc;
	enum procmgr_boot_mode *bootmode;
};

bool
s_procmgr_get_attach_params(int handle,
			    void *proc,
			    enum procmgr_boot_mode *bootmode)
{
	struct procmgr_get_attach_params args = {
		.proc = proc,
		.bootmode = bootmode,
	};

	if (ioctl(handle, PROCMGR_GETATTACHPARAMS, &args) < 0)
		return false;

	return args.status == 0;
}

static inline bool
s_map(int handle,
      size_t size,
      off_t offset,
      void **base)
{
	/* Align the physical address to page boundary */
	size = size + MOD(offset);
	off_t addr = offset - MOD(offset);

	*base = mmap(NULL, size,
		     PROT_READ | PROT_WRITE, MAP_SHARED,
		     handle, addr);

	if (*base == MAP_FAILED)
		return false;

	/* Change the user address to reflect the actual user address of
	 * memory block mapped. This is done since during mmap memory block
	 * was shifted (+-) so that it is aligned to page boundary. (sic)
	 */
	*base = *base + MOD(offset);

	return true;
}

static inline bool
s_unmap(uintptr_t base, size_t size)
{
	size = size + MOD(base);
	base = base - MOD(base);

	void *b = (void *) base;
	if (munmap(b, size) != 0)
		return false;

	return true;
}

bool
s_procmgr_map_memory_region(int handle,
			    struct procmgr_proc_info *info)
{
	struct procmgr_addr_info *e;

	for (int i = 0; i < info->numentries; i++) {
		e = &info->entries[i];

		void *base;
		if (!s_map(handle, e->size, e->addr[1], &base))
			return false;

		e->addr[1] = (uintptr_t) base;
		e->is_init = true;
	}

	return true;
}

struct procmgr_attach {
	int status;
	void *proc;
	enum procmgr_boot_mode *bootmode;
	struct procmgr_proc_info info;
};

bool
s_procmgr_attach(int handle,
		 void *proc,
		 enum procmgr_boot_mode *bootmode,
		 struct procmgr_proc_info *info)
{
	struct procmgr_attach args = {
		.proc = proc,
		.bootmode = bootmode,
	};

	if (ioctl(handle, PROCMGR_ATTACH, &args) < 0)
		return false;

	memcpy(info, &args.info, sizeof(args.info));

	return args.status == 0;
}

bool
s_procmgr_unmap_memory_region(struct procmgr_proc_info *info)
{
	struct procmgr_addr_info *e;

	for (int i = 0; i < info->numentries; i++) {
		e = &info->entries[i];

		uintptr_t base = e->addr[1];
		if (!e->is_init || (void *) base == MAP_FAILED)
			continue;

		if (!s_unmap(base, e->size))
			return false;

		e->is_init = false;
	}

	return true;
}

struct procmgr_detach {
	int status;
	void *proc;
	struct procmgr_proc_info info;
};

bool
s_procmgr_detach(int handle, void *proc)
{
	struct procmgr_detach args = {
		.proc = proc,
	};

	if (ioctl(handle, PROCMGR_DETACH, &args) < 0)
		return false;

	return args.status == 0;
}

bool
s_procmgr_map_sharedregions(int handle,
			    uint16_t num_entries,
			    struct sharedregion_region regions[])
{
	bool ret = true;

	for (uint16_t i = 0; i < num_entries; i++) {
		struct sharedregion_entry *e;
		e = &(regions[i].entry);

		if (e && e->is_valid) {
			void *b;
			if (!s_map(handle,
				   e->len,
				   (off_t) e->base,
				   &b))
				ret = false;
			else
				e->base = (uintptr_t) b;
		}
	}

	return ret;
}

bool
s_procmgr_unmap_sharedregions(uint16_t num_entries,
			      struct sharedregion_region regions[])
{
	bool ret = true;

	for (uint16_t i = 0; i < num_entries; i++) {
		struct sharedregion_entry *e;
		e = &(regions[i].entry);

		if (e && e->is_valid) {
			if (!s_unmap(e->base,
				     e->len))
				ret = false;
		}
	}

	return ret;
}

struct rproc_start_args {
	int status;
	uint32_t start_addr;
};

bool
s_rproc_start(int handle, uint32_t addr)
{
	struct rproc_start_args args = {
		.start_addr = addr,
	};

	if (ioctl(handle, RPROC_START, &args) < 0)
		return false;

	return args.status == 0;
}

bool
s_rproc_stop(int handle)
{
	int status;

	if (ioctl(handle, RPROC_STOP, &status) < 0)
		return false;

	return status == 0;
}

struct rproc_regevent_args {
	int status;
	uint16_t id;
	int32_t fd;
	enum procmgr_event_type event_type;
};

bool
s_rproc_register_event(int handle,
		       uint16_t id,
		       int32_t fd,
		       enum procmgr_event_type event_type)
{
	struct rproc_regevent_args args = {
		.id = id,
		.fd = fd,
		.event_type = event_type,
	};

	if (ioctl(handle, RPROC_IOCREGEVENT, &args) < 0)
		return false;

	return args.status == 0;
}

bool
s_rproc_unregister_event(int handle,
			 uint16_t id,
			 int32_t fd,
			 enum procmgr_event_type event_type)
{
	struct rproc_regevent_args args = {
		.id = id,
		.fd = fd,
		.event_type = event_type,
	};

	if (ioctl(handle, RPROC_IOCREGEVENT, &args) < 0)
		return false;

	return args.status == 0;
}


/* bring them on! */
#include "memregions.h"

/*
 *	'iova':	device iommu virtual address
 *	'da':	alias of 'iova'
 *	'pa':	physical address
 *	'va':	mpu virtual address
 */

struct mem_entry {
	uint32_t da;
	uint32_t va;
	uint32_t len;
};

static const struct mem_entry mem_regions[] = {
    { DUCATI_BOOTVECS_ADDR,
      PHYS_BOOTVECS_ADDR,
      DUCATI_BOOTVECS_LEN },
    { DUCATI_MEM_CODE_SYSM3_ADDR,
      PHYS_MEM_CODE_SYSM3_ADDR,
      DUCATI_MEM_CODE_SYSM3_LEN },
    { DUCATI_MEM_CODE_APPM3_ADDR,
      PHYS_MEM_CODE_APPM3_ADDR,
      DUCATI_MEM_CODE_APPM3_LEN },
    { DUCATI_MEM_HEAP1_APPM3_ADDR,
      PHYS_MEM_HEAP1_APPM3_ADDR,
      DUCATI_MEM_HEAP1_APPM3_LEN },
    { DUCATI_MEM_CONST_SYSM3_ADDR,
      PHYS_MEM_CONST_SYSM3_ADDR,
      DUCATI_MEM_CONST_SYSM3_LEN },
    { DUCATI_MEM_HEAP_SYSM3_ADDR,
      PHYS_MEM_HEAP_SYSM3_ADDR,
      DUCATI_MEM_HEAP_SYSM3_LEN },
    { DUCATI_MEM_CONST_APPM3_ADDR,
      PHYS_MEM_CONST_APPM3_ADDR,
      DUCATI_MEM_CONST_APPM3_LEN },
    { DUCATI_MEM_HEAP2_APPM3_ADDR,
      PHYS_MEM_HEAP2_APPM3_ADDR,
      DUCATI_MEM_HEAP2_APPM3_LEN },
    { DUCATI_MEM_TRACEBUF_ADDR,
      PHYS_MEM_TRACEBUF_ADDR,
      DUCATI_MEM_TRACEBUF_LEN },
    { DUCATI_MEM_IPC_HEAP0_ADDR,
      PHYS_MEM_IPC_HEAP0_ADDR,
      DUCATI_MEM_IPC_HEAP0_LEN },
    { DUCATI_MEM_IPC_HEAP1_ADDR,
      PHYS_MEM_IPC_HEAP1_ADDR,
      DUCATI_MEM_IPC_HEAP1_LEN },

    { TESLA_MEM_CODE_DSP_ADDR,
      PHYS_MEM_CODE_DSP_ADDR,
      TESLA_MEM_CODE_DSP_LEN },
    { TESLA_MEM_CONST_DSP_ADDR,
      PHYS_MEM_CONST_DSP_ADDR,
      TESLA_MEM_CONST_DSP_LEN },
    { TESLA_MEM_HEAP_DSP_ADDR,
      PHYS_MEM_HEAP_DSP_ADDR,
      TESLA_MEM_HEAP_DSP_LEN },
    { TESLA_MEM_IPC_HEAP0_ADDR,
      PHYS_MEM_IPC_HEAP0_ADDR,
      TESLA_MEM_IPC_HEAP0_LEN },
    { TESLA_MEM_IPC_HEAP1_ADDR,
      PHYS_MEM_IPC_HEAP1_ADDR,
      TESLA_MEM_IPC_HEAP1_LEN },
};

static uint32_t
translate_address(uint32_t addr)
{
	for (uint i = 0; i < ARRAY_SIZE(mem_regions); i++) {
		if (addr >= mem_regions[i].da
		    && addr < mem_regions[i].da + mem_regions[i].len) {
			uint32_t offset = addr - mem_regions[i].da;
			return mem_regions[i].va + offset;
		}
	}

	return 0;
}

bool
s_procmgr_load_elf_segment(int handle,
			   FILE *image,
			   uint32_t vaddr,
			   size_t memsz,
			   size_t filesz,
			   long int offset)
{
	void *base = NULL;

	if (!s_map(handle, memsz,
		   translate_address(vaddr),
		   &base))
		return false;

	memset(base, 0, memsz);
	if (fseek(image, offset, SEEK_SET) != 0)
		return false;
	if (fread(base, filesz, 1, image) != 1)
		return false;

	if (!s_unmap((uintptr_t) base, memsz))
		return false;

	return true;
}

struct heapbufmp_args {
	void *cfg;
	int32_t data[4];
	int32_t status;
};

bool
s_heapbufmp_params_init(int handle,
			struct heapbufmp_params *params)
{
	struct heapbufmp_args args = {
		.cfg = params,
	};
	if (ioctl(handle, HEAPBUFMP_PARAMSINIT, &args) < 0)
		return false;

	return args.status == 0;
}

struct heapbufmp_gsmrs_args {
	void *handle;
	void *params;
	uint32_t shared_addr;
	uint32_t size;
	uint32_t data;
	int32_t status;
};

bool
s_heapbufmp_get_sharedmem_requested_size(int handle,
					 struct heapbufmp_params *params,
					 uint32_t *size)
{
	struct heapbufmp_gsmrs_args args = {
		.params = params,
	};
	if (ioctl(handle, HEAPBUFMP_SHAREDMEMREQ, &args) < 0)
		return false;

	*size = args.size;

	return args.status == 0;
}

struct heapbufmp_create_args {
	void *handle;
	void *params;
	uint32_t name_len;
	uint32_t sr_addr;
	void *gate;
	int32_t status;
};

bool
s_heapbufmp_create(int handle,
		   struct heapbufmp_params *params,
		   uint32_t sr_addr,
		   void **heap)
{
	uint32_t name_len = 0;

	if (params->name)
		name_len = strlen(params->name) + 1;

	struct heapbufmp_create_args args = {
		.params = params,
		.sr_addr = sr_addr,
		.name_len = name_len,
		.gate = params->gate,
	};
	if (ioctl(handle, HEAPBUFMP_CREATE, &args) < 0)
		return false;

	*heap = args.handle;

	return args.status == 0;
}

struct heapbufmp_delete_args {
	void *handle;
	int32_t data[4];
	int32_t status;
};

bool
s_heapbufmp_delete(int handle, void *heap)
{
	struct heapbufmp_delete_args args = {
		.handle = heap,
	};
	if (ioctl(handle, HEAPBUFMP_DELETE, &args) < 0)
		return false;

	return args.status == 0;
}

struct heapmemmp_alloc_args {
	void *heap;
	uint32_t size;
	uint32_t align;
	uint32_t sr_addr;
	int32_t data[2];
	int32_t status;
};

bool
s_heapmemmp_alloc(int handle,
		  void *heap,
		  uint32_t size,
		  uint32_t align,
		  uint32_t *sr_addr)
{
	struct heapmemmp_alloc_args args = {
		.heap = heap,
		.size = size,
		.align = align,
	};
	if (ioctl(handle, HEAPMEMMP_ALLOC, &args) < 0)
		return false;

	*sr_addr = args.sr_addr;

	return args.status == 0;
}

struct heapmemmp_free_args {
	void *heap;
	uint32_t sr_addr;
	uint32_t size;
	int32_t data[3];
	int32_t status;
};

bool
s_heapmemmp_free(int handle,
		 void *heap,
		 uint32_t size,
		 uint32_t sr_addr)
{
	struct heapmemmp_free_args args = {
		.heap = heap,
		.sr_addr = sr_addr,
		.size = size,
	};
	if (ioctl(handle, HEAPMEMMP_FREE, &args) < 0)
		return false;

	return args.status == 0;
}

enum ipc_proc_sync {
	IPC_PROC_SYNC_NONE, /* don't do any processor sync */
	IPC_PROC_SYNC_PAIR, /* sync pair of processors in ipc_attach */
	IPC_PROC_SYNC_ALL,  /* sync all processors, done in ipc_start */
};

struct ipc_args {
	union {
		enum ipc_proc_sync *cfg;
		struct {
			uint16_t id;
			int32_t cmd;
			uint32_t arg;
		} control;
	} args;
	int32_t data;
	int32_t status;
};

bool
s_ipc_setup(int handle)
{
	enum ipc_proc_sync cfg = IPC_PROC_SYNC_PAIR;
	struct ipc_args args = {
		.args.cfg = &cfg,
	};
	if (ioctl(handle, IPC_SETUP, &args) < 0)
		return false;

	return args.status >= 0;
}

bool
s_ipc_destroy(int handle)
{
	struct ipc_args args = {
		.args.cfg = NULL,
	};
	if (ioctl(handle, IPC_DESTROY, &args) < 0)
		return false;

	return args.status == 0;
}

bool
s_ipc_control(int handle, uint16_t id, int32_t cmd, uint32_t arg)
{
	struct ipc_args args = {
		.args.control.arg = arg,
		.args.control.cmd = cmd,
		.args.control.id = id,
	};
	if (ioctl(handle, IPC_CONTROL, &args))
		return false;

	return args.status == 0;
}

enum page_type {
	SECTION, LARGE_PAGE, SMALL_PAGE, SUPER_SECTION,
};

static bool
iovmm_get_entry_size(uint32_t pa,
		     uint32_t len,
		     enum page_type *type,
		     uint32_t *size)
{
	int i;
	static uint32_t page_size[] = { SZ_16M,
					SZ_1M,
					SZ_64K,
					SZ_4K };

	for (i = 0; i < 4 && pa % page_size[i]; i++) ;
	if (i == 4)
		return false; /* invalid page alignment */

	uint32_t msize = len < page_size[i]
		? len
		: page_size[i];

	if (msize >= SZ_16M) {
		*type = SUPER_SECTION;
		*size = SZ_16M;
	} else if (msize >= SZ_1M) {
		*type = SECTION;
		*size = SZ_1M;
	} else if (msize >= SZ_64K) {
		*type = LARGE_PAGE;
		*size = SZ_64K;
	} else if (msize >= SZ_4K) {
		*type = SMALL_PAGE;
		*size = SZ_4K;
	} else
		return false;

	return true;
}

struct iotbl_entry {
	uint32_t da;
	uint32_t pa;
	uint32_t pgsz;
	uint32_t prsvd;
	uint32_t valid;
	uint32_t endian;
	uint32_t elsz;
	uint32_t mixed;
};

/*
 * set an iommu tbl entry
 */
static bool
dmm_add_entry(int handle,
	      uint32_t *pa,
	      uint32_t *da,
	      uint32_t size)
{
	uint32_t mapped_size = 0;
	uint32_t esize;
	enum page_type type;

	while (mapped_size < size) {
		if (!iovmm_get_entry_size(*pa,
					  size - mapped_size,
					  &type,
					  &esize))
			return false;

		struct iotbl_entry e = {
			.elsz   = 1 << 7,
			.endian = 0 << 9,
			.mixed  = 1 << 6,
			.prsvd  = 1 << 3,
			.valid  = 1 << 2,
			.da = *da,
			.pa = *pa,
		};

		if (type == SUPER_SECTION)
			e.pgsz = 3 << 0;
		else if (type == SECTION)
			e.pgsz = 0 << 0;
		else if (type == LARGE_PAGE)
			e.pgsz = 1 << 0;
		else if (type == SMALL_PAGE)
			e.pgsz = 2 << 0;

		if (!ioctl(handle, DMM_IOCSETTLBENT, &e) < 0)
			return false;

		mapped_size += esize;
		*pa += esize;
		*da += esize;
	}

	return true;
}

struct dmm_map_info {
	uint32_t va;
	uint32_t *da;
	uint32_t num_of_buf;
	uint32_t size;
	uint32_t pool_id;
	uint32_t flags;
};

static bool
dmm_map(int handle,
	uint32_t va,
	uint32_t *da,
	uint32_t num_of_buf,
	uint32_t size,
	uint32_t pool_id,
	uint32_t flags)
{
	struct dmm_map_info info = {
		.va = va,
		.da = da,
		.num_of_buf = num_of_buf,
		.size = size,
		.pool_id = pool_id,
		.flags = flags,
	};

	if (!ioctl(handle, DMM_IOCMEMMAP, &info) < 0)
		return false;

	return true;
}

struct mmu_entry {
	uint32_t pa;
	uint32_t va;
	uint32_t size;
};

/*
 * The L4 is composed of the following:
 *
 * - L4_CFG: Includes the majority of the configuration interface for
 *   L3 system modules and peripheral interconnect; L4_PER: Includes
 *   the main peripherals that require system direct memory access
 *   (sDMA) access.
 *
 * - L4_WKUP: Includes peripherals attached to the WKUP power
 *   domain.
 */
static const struct mmu_entry l4map[] = {
	/* TILER 8-bit and 16-bit modes */
	{ 0x60000000, 0x60000000, SZ_16M * 16 },
	/* TILER 32-bit mode */
	{ 0x70000000, 0x70000000, SZ_16M * 8 },
	/* TILER: Pages-mode */
	{ 0x78000000, 0x78000000, SZ_16M * 8 },
	/* L4_CFG: Covers all modules in L4_CFG SZ_16M */
	{ 0x4A000000, 0xAA000000, SZ_16M },
	/*  L4_PER: Covers all modules in L4_PER 16M */
	{ 0x48000000, 0xA8000000, SZ_16M },
	/* IVA_HD Config: Covers all modules in IVA_HD Config space 16M */
	{ 0x5A000000, 0xBA000000, SZ_16M },
	/* IVA_HD SL2: Covers all memory in IVA_HD SL2 space 16M */
	{ 0x5B000000, 0xBB000000, SZ_16M },
	/* L4_EMU: to enable STM*/
	{ 0x54000000, 0xB4000000, SZ_16M },
};

struct memory_entry {
	uint32_t va;
	uint32_t size;
};

/*
 * L3 handles many types of data transfers, especially exchanges with
 * system-on-chip (SoC)/external memories. L3 transfers data with a
 * maximum width of 64 bits from the initiator to the target. The L3
 * interconnect is a little-endian platform.
 * --TRM
 */
static const struct memory_entry l3mreg[] = {
	{ DUCATI_MEM_IPC_HEAP0_ADDR, SZ_1M },
	{ 0, SZ_16M },
	{ DUCATI_MEM_CONST_SYSM3_ADDR, SZ_16M },
	{ DUCATI_MEM_CONST_SYSM3_ADDR + SZ_16M, SZ_16M },
};

bool
s_dmm_mpu_init(int handle)
{
	uint32_t va, pa, siz;

	pa = 0x9cf00000; /* ducati base-image physical address */
	for (uint i = 0; i < ARRAY_SIZE(l3mreg); i++) {
		va = l3mreg[i].va;
		siz = l3mreg[i].size;

		if (i > 3) {
			/* map from da to pa */
			if (!dmm_map(handle, pa, &va, 1, siz, -1, 0x2))
				return false;
			pa += siz;
		} else {
			if (!dmm_add_entry(handle, &pa, &va, siz))
				return false;
		}
	}

	for (uint i = 0; i < ARRAY_SIZE(l4map); i++) {
		va = l4map[i].va;
		pa = l4map[i].pa;
		siz = l4map[i].size;
		/* map from da to pa */
		if (!dmm_map(handle, pa, &va, 1, siz, -1, 0x2))
			return false;
	}

	return true;
}

struct iovmm_pool_info {
	uint32_t pool_id;
	uint32_t da_begin;
	uint32_t da_end;
	uint32_t size;
	uint32_t flags;
};

bool
s_dmm_create_pool(int handle,
		  uint32_t pool_id,
		  uint32_t da_begin,
		  uint32_t da_end,
		  uint32_t size,
		  uint32_t flags)
{
	struct iovmm_pool_info info = {
		.pool_id = pool_id,
		.size = size,
		.da_begin = da_begin,
		.da_end = da_end,
		.flags = flags
	};

	if (ioctl(handle, DMM_IOCCREATEPOOL, &info) < 0)
		return false;

	return true;
}

bool
s_dmm_delete_pool(int handle,
		  uint32_t pool_id)
{
	if (ioctl(handle, DMM_IOCDELETEPOOL, &pool_id) < 0)
		return false;

	return true;
}

bool
s_mmu_register_event(int handle, int fd)
{
	if (ioctl(handle, IOMMU_REGEVENT, &fd) < 0)
		return false;

	return true;
}

bool
s_mmu_unregister_event(int handle, int fd)
{
	if (ioctl(handle, IOMMU_UNREGEVENT, &fd) < 0)
		return false;

	return true;
}

struct deh_args {
	int32_t fd;
	enum deh_event_type event_type;
};

bool
s_deh_register_event(int handle,
		     enum deh_event_type event_type,
		     int fd)
{
	struct deh_args args = {
		.fd = fd,
		.event_type = event_type,
	};

	if (ioctl(handle, DEH_REGEVENT, &args) < 0)
		return false;

	return true;
}

bool
s_deh_unregister_event(int handle,
		       enum deh_event_type event_type,
		       int fd)
{
	struct deh_args args = {
		.fd = fd,
		.event_type = event_type,
	};

	if (ioctl(handle, DEH_UNREGEVENT, &args) < 0)
		return false;

	return true;
}
