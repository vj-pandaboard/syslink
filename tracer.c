/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#include <stdlib.h>
#include <unistd.h> /* for usleep */
#include <stdio.h>  /* for printf */
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <sys/mman.h> /* for mmap */
#include <signal.h> /* for signal */
#include <fcntl.h>
#include <errno.h>
#include <time.h>

#include "util.h"
#include "log.h"

bool finish = false;

static inline bool
map(int handle,
    size_t size,
    off_t offset,
    void **base)
{
	*base = mmap(NULL, size,
		     PROT_READ | PROT_WRITE, MAP_SHARED,
		     handle, offset);

	if (*base == MAP_FAILED)
		return false;

	return true;
}

static inline bool
unmap(void *base,
      size_t size)
{
	if (munmap(base, size) != 0)
		return false;

	return true;
}

static void
get_trace(uint32_t rptr,
	  uint32_t wptr,
	  const char *str,
	  char *trace)
{
	uint32_t idx = 0;
	uint32_t to_copy;

	if (rptr < wptr) {
		to_copy = wptr - rptr;
		memcpy(trace, &str[rptr], to_copy);
		idx += to_copy;
	} else {
		to_copy = 0x10000 - 0x8 - rptr;
		memcpy(trace, &str[rptr], to_copy);
		idx += to_copy;
		to_copy = wptr;
		memcpy(&trace[idx], str, to_copy);
		idx += to_copy;
	}

	trace[idx] = '\0';
}

static void
print_trace(const char *log)
{
	fprintf(stdout, "%s", log);
	fflush(stdout);
}

static bool
wait_for_change(volatile uint32_t *a,
		volatile uint32_t *b)
{
	struct timespec ts = {
		.tv_nsec = 500000000, /* 1/2 sec */
	};

	while(*a == *b) {
		if (nanosleep(&ts, NULL) == -1) {
			pr_warning("%s", strerror(errno));
			goto bail;
		}
	}

	return *a != *b;

bail:
	finish = true;
	return false;
}

static void
loop(void *base)
{
	volatile uint32_t *rptr;
	volatile uint32_t *wptr;
	char *str, buf[0x10080];

	rptr = base;
	wptr = base + sizeof(uint32_t);
	str = base + 2 * sizeof(uint32_t);

	*rptr = *wptr = 0;

	while (!finish) {
		if (wait_for_change(rptr, wptr)) {
			get_trace(*rptr, *wptr, str, buf);
			print_trace(buf);

			/* update the read position */
			*rptr = *wptr;
		}
	}
}

static bool
trace(int handle,
      off_t offset)
{
	void *base;

	if (!map(handle,
		 0x10000,
		 offset,
		 &base))
		return false;

	loop(base);

	if (!unmap(base,
		   0x10000))
		return false;

	return true;
}

static void
termination_handler(int signum)
{
	finish = true;
}

int
main(int argc, char **argv)
{
	int ret = EXIT_FAILURE;

	{
		signal(SIGINT, termination_handler);
		signal(SIGTERM, termination_handler);
	}

	int proc_handle = open("/dev/syslink-procmgr", O_SYNC | O_RDWR);
	if (proc_handle == -1)
		return EXIT_FAILURE;

	/* APP */
	if (!trace(proc_handle,
		   0x9fff0000))
		goto bail;

	ret = EXIT_SUCCESS;

bail:
	close(proc_handle);

	return ret;
}
