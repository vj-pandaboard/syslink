CROSS_COMPILE ?= arm-linux-
CC := $(CROSS_COMPILE)gcc

CFLAGS := -O0 -ggdb -Wall -Wextra -Wno-unused-parameter \
	-Wmissing-prototypes -ansi

override CFLAGS += -std=gnu99 -D_GNU_SOURCE

ifdef DEBUG
override CFLAGS += -DDEBUG
endif

GST_CFLAGS := $(shell pkg-config --cflags gstreamer-0.10)
GST_LIBS := $(shell pkg-config --libs gstreamer-0.10)

all:

version := $(shell ./get-version)

daemon: daemon.o elf.o syslink.o log.o
daemon: LIBS += -lelf -lpthread
bins += daemon

dce-test: dce-test.o syslink.o rcm.o dce.o log.o tiler.o viddec.o
dce-test: LIBS += -lpthread
bins += dce-test

tracer: tracer.o log.o
bins += tracer

gst_plugin := libgstsyslink.so

$(gst_plugin): plugin.o gstsyslinkvdec.o syslink.o rcm.o dce.o log.o tiler.o \
	viddec.o
$(gst_plugin): override CFLAGS += $(GST_CFLAGS) \
	-DVERSION='"$(version)"' -DGST_DISABLE_DEPRECATED
$(gst_plugin): override LIBS += $(GST_LIBS)

all: $(gst_plugin) $(bins)

D = $(DESTDIR)

# pretty print
ifndef V
QUIET_CC    = @echo '   CC         '$@;
QUIET_LINK  = @echo '   LINK       '$@;
QUIET_CLEAN = @echo '   CLEAN      '$@;
endif

.PHONY: push

%.so: override CFLAGS += -fPIC

%.o:: %.c
	$(QUIET_CC)$(CC) $(CFLAGS) $(INCLUDES) -MMD -o $@ -c $<

$(bins):
	$(QUIET_LINK)$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

%.so::
	$(QUIET_LINK)$(CC) $(LDFLAGS) -shared $^ $(LIBS) -o $@

clean:
	$(QUIET_CLEAN)$(RM) $(bins) $(gst_plugin) *.o *.d

push: all
	rsync -avC --include=*.so -e ssh --delete ./ $(PANDA):~/syslink

-include *.d
