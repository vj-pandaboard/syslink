/*
 * Copyright (C) 2011 Víctor Manuel Jáquez Leal
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

/* Tiled memory manager */

#ifndef TILER_H
#define TILER_H

#include <stdbool.h>
#include <stdint.h>

enum tiler_fmt {
	TILFMT_MIN     = -2,
	TILFMT_INVALID = -2,
	TILFMT_NONE    = -1,
	TILFMT_8BIT    = 0,
	TILFMT_16BIT   = 1,
	TILFMT_32BIT   = 2,
	TILFMT_PAGE    = 3,
	TILFMT_MAX     = 3,
	TILFMT_8AND16  = 4,
};

struct tiler_block_info {
	enum tiler_fmt fmt;
	uint32_t len;
	uint32_t stride;
	void *ptr;
	uint32_t id;
	uint32_t key;
	uint32_t group_id;

	/* alignment requirements for ssptr: ssptr & (align - 1) == offs */
	uint32_t align;
	uint32_t offs;
	uint32_t ssptr;
};

struct tiler_buffer_info {
	uint32_t num_blocks;
	struct tiler_block_info blocks[16];
	uint32_t off;
	uint32_t len;
};

int tiler_open(void);
bool tiler_alloc(int handle,
		 struct tiler_buffer_info *buffer);
bool tiler_free(int handle,
		struct tiler_buffer_info *buffer);
bool tiler_get_pa(int handle,
		  void *vaddr,
		  uintptr_t *paddr);

#endif /* TILER_H */
