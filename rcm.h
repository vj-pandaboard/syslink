/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

/* Remote Command Messaging */

#ifndef RCM_H
#define RCM_H

#include <stdbool.h>
#include <stdint.h>

#include "list.h"

struct rcm_client {
	/* data provided by the user */
	int ipc_handle;
	uint16_t sr_nentries;
	struct sharedregion_region *regions;

	/* queues */
	uint32_t queue_id;
	void *msg_queue;
	uint32_t msg_queue_id;
	void *err_queue;
	uint32_t err_queue_id;

	/* internal variables */
	struct list_head mailbox;
};

struct rcm_client_msg {
	uint16_t pool_id;
	uint16_t job_id;
	uint32_t func_idx;
	int32_t res;
	uint32_t size;
	uintptr_t data[1];
};

struct rcm_client *rcm_client_new(int ipc_handle);
void rcm_client_free(struct rcm_client *rcm);
bool rcm_client_init(struct rcm_client *self, char *name);
bool rcm_client_deinit(struct rcm_client *self);
bool rcm_client_alloc(struct rcm_client *self,
		      uint32_t size,
		      struct rcm_client_msg **msg);
bool rcm_client_free_msg(struct rcm_client *self,
			 struct rcm_client_msg *msg);
bool rcm_client_get_symbol_index(struct rcm_client *self,
				 const char *name,
				 uint32_t *index);
bool rcm_client_exec(struct rcm_client *self,
		     struct rcm_client_msg *in,
		     struct rcm_client_msg **out);

#endif /* RCM_H */
