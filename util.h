/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#ifndef UTIL_H
#define UTIL_H

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

#define PAGE_SIZE 0x1000 /* 4K */
#define MOD(num) ((num) & (PAGE_SIZE - 1))
#define POWERDOWN(num) ((num) & ~(PAGE_SIZE - 1))
#define POWERUP(num) POWERDOWN((num) + (PAGE_SIZE - 1))

#define ALIGN(x,n)   (((x) + ((1 << (n)) - 1)) & ~((1 << (n)) - 1))

#define MIN(a,b)     (((a) < (b)) ? (a) : (b))

#endif /* UTIL_H */
