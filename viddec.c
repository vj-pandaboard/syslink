/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#include <stddef.h>
#include <string.h>

#include "viddec.h"

/**
 * Defines the creation time parameters for all video decoders
 * instance objects.
 */
struct viddec_params {
	int32_t size;             /**< @sizeField */
	int32_t max_height;       /**< Maximum video height in pixels. */
	int32_t max_width;        /**< Maximum video width in pixels. */
	int32_t max_framerate;    /**< Maximum frame rate in fps * 1000.
				   *   For example, if max frame rate is 30
				   *   frames per second, set this field
				   *   to 30000.
				   */
	int32_t max_bitrate;      /**< Maximum bit rate, bits per second.
				   *   For example, if bit rate is 10 Mbps, set
				   *   this field to 10000000
				   */
	int32_t data_endianness;  /**< Endianness of output data. */
	int32_t force_chroma_format;
	int32_t operating_mode;   /**< Video coding mode of operation. */
	int32_t display_delay;
	int32_t input_data_mode;  /**< Input data mode.
				   *
				   * @remarks If a subframe mode is provided,
				   *          the application must call
				   *          IVIDDEC3_Fxns.control() with
				   *          #XDM_SETPARAMS id prior to
				   *          IVIDDEC3_Fxns.process() to
				   *          set
				   *          IVIDDEC3_DynamicParams.getDataFxn()
				   *          and
				   *          IVIDDEC3_DynamicParams.getDataHandle
				   *          (and optionally
				   *          IVIDDEC3_DynamicParams.putBufferFxn(),
				   *          and
				   *          IVIDDEC3_DynamicParams.putBufferHandle).
				   *          Else, the alg can return error.
				   */
	int32_t output_data_mode;  /**< Output data mode.
				    *
				    * @remarks If a subframe mode is provided,
				    *          the application must call
				    *          IVIDDEC3_Fxns.control() with
				    *          #XDM_SETPARAMS id prior to
				    *          #IVIDDEC3_Fxns.process() to
				    *          set
				    *          IVIDDEC3_DynamicParams.putDataFxn(),
				    *          and
				    *          IVIDDEC3_DynamicParams.putDataHandle.
				    *          Else, the alg can return error.
				    */
	int32_t num_input_data_units;/**< Number of input slices/rows.
				      *
				      *   @remarks   Units depend on the
				      *              IVIDDEC3_Params.inputDataMode,
				      *              like number of
				      *              slices/rows/blocks etc.
				      *
				      *   @remarks   Ignored if
				      *              IVIDDEC3_Params.inputDataMode
				      *              is set to full frame mode.
				      */
	int32_t num_output_data_units;/**< Number of output slices/rows.
				       *
				       *   @remarks   Units depend on the
				       *              IVIDDEC3_Params.outputDataMode,
				       *              like number of
				       *              slices/rows/blocks etc.
				       *
				       *   @remarks   Ignored if
				       *              IVIDDEC3_Params.outputDataMode
				       *              is set to full frame mode.
				       */
	int32_t error_info_mode;   /**< Enable/disable packet error information
				    *   for input and/or output.
				    *
				    *   @sa IVIDEO_ErrorInfoMode
				    */
	int32_t display_bufs_mode; /**< Indicates which mode the displayBufs are
				    *   presented in.
				    *
				    *   @remarks   See the
				    *              IVIDDEC3_DisplayBufsMode enum
				    *              for the values this field may
				    *              contain.
				    *
				    *   @sa IVIDDEC3_OutArgs.displayBufsMode
				    *   @sa IVIDDEC3_DisplayBufsMode
				    */
	int32_t metadata_type[3]; /**< Type of
				   *   each metadata plane.
				   *
				   *   @sa IVIDEO_MetadataType
				   */
};

bool
viddec_set_params(struct codec_engine *ce,
		  uint32_t height,
		  uint32_t width,
		  int32_t size,
		  void **data)
{
	struct viddec_params params = {
		.size = size,
		.max_width = width,
		.max_height = height,
		.max_framerate = 30000,
		.max_bitrate = 10000000,
		.data_endianness = 1,            /* big endian */
		.force_chroma_format = 9,        /* YUV 420
						  * semiplanar */
		.operating_mode = 0,             /* decode only */
		.display_delay = -1,             /* decoder decides
						  * the display
						  * delay */
		.display_bufs_mode = 1,          /* details are embedded */
		.input_data_mode = 3,            /* processing of
						  * entire frame
						  * data */
		.metadata_type  = { -1, -1, -1}, /* no metadata */
		.num_input_data_units = 0,
		.output_data_mode = 3,           /* processing of
						  * entire frame
						  * data */
		.num_output_data_units = 0,
		.error_info_mode = 0,
	};

	if (!dce_buffer_alloc(ce, size, data))
		return false;

	memcpy(*data, &params, sizeof(params));
	return true;
}

/**
 *  @brief          Descriptor for the chunk of data being
 *                  transferred in one call to putData or getData
 */
struct data_sync_desc {
	int32_t size;            /**< @sizeField */
	int32_t scattered_blocks_flag; /**< Flag indicating whether the
					*   individual data blocks may
					*   be scattered in memory.
					*
					*   @remarks   Note that each individual
					*              block must be physically
					*              contiguous.
					*
					*   @remarks   Valid values are XDAS_TRUE
					*              and XDAS_FALSE.
					*
					*   @remarks   If set to XDAS_FALSE, the
					*              @c baseAddr field points
					*              directly to the start of the
					*              first block, and is not treated
					*              as a pointer to an array.
					*
					*   @remarks   If set to XDAS_TRUE, the
					*              @c baseAddr array must
					*              contain the base address of
					*              each individual block.
					*/
	int32_t *base_addr;       /**< Base address of single data block or
				   *              pointer to an array of
				   *              data block addresses of
				   *              size @c numBlocks.
				   *
				   *   @remarks   If @c scatteredBlocksFlag is
				   *              set to XDAS_FALSE, this
				   *              field points
				   *              directly to the start of the
				   *              first block, and is not treated
				   *              as a pointer to an array.
				   *
				   *   @remarks   If @c scatteredBlocksFlag is
				   *              set to XDAS_TRUE, this
				   *              field points to an array of
				   *              pointers to the data blocks.
				   */
	int32_t num_blocks;       /**< Number of blocks available */
	int32_t var_block_sizes_flag; /**< Flag indicating whether any of the
				       *   data blocks vary in size.
				       *
				       *   @remarks   Valid values are XDAS_TRUE
				       *              and XDAS_FALSE.
				       */
	int32_t *block_sizes;     /**< Variable block sizes array.
				   *
				   *  @remarks     If @c varBlockSizesFlag is
				   *               XDAS_TRUE, this array
				   *               contains the sizes of each
				   *               block.  If @c varBlockSizesFlag
				   *               is XDAS_FALSE, this contains
				   *               the size of same-size blocks.
				   *
				   *   @remarks    Memory for this array
				   *               (of size @c numBlocks) has
				   *               to be allocated by the
				   *               caller of the putData API.
				   */
};

/**
 *  @brief      Non-blocking API to signal "data ready" to one or more
 *              consumers.
 *
 *  @param[in]  dataSyncHandle  Handle to a data sync instance.
 *  @param[out] dataSyncDesc    Full data sync descriptor.  This includes one
 *                              or more filled data buffers.
 *
 *
 *  @todo       Needs review
 *
 *  @sa IVIDDEC3_DynamicParams::putDataFxn()
 *  @sa IVIDENC2_DynamicParams::putDataFxn()
 *  @sa IVIDENC2_DynamicParams::getBufferFxn()
 *  @sa XDM_DataSyncGetBufferFxn
 */
typedef void (*DataSyncPutFxn)(void *handle,
			       struct data_sync_desc *data_sync_desc);

/**
 *  @brief          API to obtain data information from a consumer.
 *
 *  @param[in]  dataSyncHandle  Handle to a data sync instance.
 *  @param[out] dataSyncDesc    Empty data sync descriptor to be filled by
 *                              the producer.  Only the @c size field must
 *                              be initialized by the caller.
 *
 *  @post       Upon return, the @c dataSyncDesc will contain details about
 *              the provided data.
 *
 *  @remarks    Given that this is an input buffer, the implementation of this
 *              fxn must make the provided external or shared memory coherent
 *              with respect to the algorithm processor's cache.
 *
 *  @todo       Needs review
 *
 *  @sa IVIDDEC3_DynamicParams::getDataFxn()
 *  @sa IVIDDEC3_DynamicParams::putBufferFxn()
 *  @sa IVIDENC2_DynamicParams::getDataFxn()
 *  @sa XDM_DataSyncPutBufferFxn
 */
typedef int32_t (*DataSyncGetFxn)(void *handle,
				  struct data_sync_desc *data_sync_desc);

/**
 *  @brief      API to obtain empty bitstream buffers from an allocator
 *              to be filled by the algorithm.
 *
 *  @param[in]  dataSyncHandle  Handle to a data sync instance.
 *  @param[out] dataSyncDesc    Empty data sync descriptor to be filled by
 *                              the allocator.  Only the @c size field must
 *                              be initialized by the caller.
 *
 *  @post       Upon return, the @c dataSyncDesc will contain details about
 *              the allocated buffer(s).
 *
 *  @remarks    Given that this is an output buffer, the implementation of this
 *              fxn (i.e., the allocator) must make the provided external or
 *              shared memory coherent with respect to the algorithm
 *              processor's cache.  This typically implies it must be cache
 *              invalidated since the alg may fill this buffer via DMA.
 *
 *  @remarks    The allocator may not zero-initialize this buffer.  If the
 *              allocator <i>does</i> initialize the buffer, it must ensure the
 *              cache coherency via writeback-invalidate.
 *
 *  @todo       Needs review
 *
 *  @sa IVIDENC2_DynamicParams::putDataFxn()
 *  @sa IVIDENC2_Fxns::process()
 */
typedef int32_t (*DataSyncGetBufferFxn)(void *handle,
					struct data_sync_desc *data_sync_desc);


/**
 *  @brief      API to return consumed bitstream buffers to the original
 *              provider.
 *
 *  @param[in]  dataSyncHandle  Handle to a data sync instance.
 *  @param[out] dataSyncDesc    Data sync descriptor.
 *
 *  @todo       What cache coherency responsibilities are placed on this
 *              buffer?
 *
 *  @todo       Needs review and further detail
 *
 *  @sa IVIDDEC3_DynamicParams::getDataFxn()
 *  @sa IVIDDEC3_Fxns::process()
 */
typedef int32_t (*DataSyncPutBufferFxn)(void *handle,
					struct data_sync_desc *data_sync_desc);

/*@}*/

/**
 *  This structure defines the algorithm parameters that can be
 *  modified after creation.
 */
struct viddec_dynparams {
	int32_t size;            /**< @sizeField */
	int32_t decode_header;    /**< @copydoc XDM_DecMode
				   *
				   *   @sa XDM_DecMode
				   */
	int32_t display_width;    /**< Pitch.  If set to zero, use the decoded
				   *   image width.  Else, use given display
				   *   width in pixels.  Display width has to be
				   *   greater than or equal to image width.
				   */
	int32_t frame_skip_mode;   /**< @copydoc IVIDEO_FrameSkip
				    *
				    *   @sa IVIDEO_FrameSkip
				    */
	int32_t new_frame_flag;    /**< Flag to indicate that the algorithm should
				    *   start a new frame.
				    *
				    *   @remarks   Valid values are TRUE
				    *              and FALSE.
				    *
				    *   @remarks   This is useful for error
				    *              recovery, for example when the
				    *              end of frame cannot be detected
				    *              by the codec but is known to the
				    *              application.
				    */
	DataSyncPutFxn put_data_fxn; /**< Optional datasync "put data" function.
				      *
				      *   @remarks   Apps/frameworks that don't
				      *              support datasync should set
				      *              this to NULL.
				      *
				      *   @remarks   This function is provided
				      *              by the app/framework to the
				      *              video decoder.  The decoder
				      *              calls this function when
				      *              sub-frame data has been put
				      *              into an output buffer and is
				      *              available.
				      */
	void *put_data_handle;/**< Datasync "put data" handle
			       *
			       *   @remarks   This is a handle which the
			       *              codec must provide when
			       *              calling the app-registered
			       *              IVIDDEC3_DynamicParams.putDataFxn().
			       *
			       *   @remarks   Apps/frameworks that don't
			       *              support datasync should set
			       *              this to NULL.
			       *
			       *   @remarks   For an algorithm, this handle
			       *              is read-only; it must not be
			       *              modified when calling
			       *              the app-registered
			       *              IVIDDEC3_DynamicParams.putDataFxn().
			       *
			       *   @remarks   The app/framework can use
			       *              this handle to differentiate
			       *              callbacks from different
			       *              algorithms.
			       */
	DataSyncGetFxn get_data_fxn;/**< Datasync "get data" function.
				     *
				     *   @remarks   This function is provided
				     *              by the app/framework to the
				     *              video decoder.  The decoder
				     *              calls this function to get
				     *              partial compressed bit-stream
				     *              data from the app/framework.
				     *
				     *   @remarks   Apps/frameworks that don't
				     *              support datasync should set
				     *              this to NULL.
				     */
	void *get_data_handle;/**< Datasync "get data" handle
			       *
			       *   @remarks   This is a handle which the
			       *              codec must provide when
			       *              calling @c getDataFxn.
			       *
			       *   @remarks   Apps/frameworks that don't
			       *              support datasync should set
			       *              this to NULL.
			       *
			       *   @remarks   For an algorithm, this handle
			       *              is read-only; it must not be
			       *              modified when calling
			       *              the app-registered
			       *              IVIDDEC3_DynamicParams.getDataFxn().
			       *
			       *   @remarks   The app/framework can use
			       *              this handle to differentiate
			       *              callbacks from different
			       *              algorithms.
			       */
	DataSyncPutBufferFxn put_buffer_fxn;/**< Datasync "put buffer" function.
					     *
					     *   @remarks This
					     *              function
					     *              is
					     *              provided
					     *              by the
					     *              app/framework
					     *              to the
					     *              video
					     *              decoder.
					     *              The
					     *              decoder
					     *              calls this
					     *              function
					     *              to release
					     *              consumed,
					     *              partial
					     *              compressed
					     *              bit-stream
					     *              data
					     *              buffers to
					     *              the
					     *              app/framework.
					     *
					     *   @remarks
					     *              Apps/frameworks
					     *              that don't
					     *              support
					     *              datasync
					     *              should set
					     *              this to
					     *              NULL.
					     */
	void *put_buffer_handle;/**< Datasync "put buffer" handle
				 *
				 *   @remarks This is a handle which
				 *              the codec must provide
				 *              when calling the
				 *              app-registered
				 *              IVIDDEC3_DynamicParam.putBufferFxn().
				 *
				 *   @remarks Apps/frameworks that
				 *              don't support datasync
				 *              should set this to
				 *              NULL.
				 *
				 *   @remarks For an algorithm, this
				 *              handle is read-only;
				 *              it must not be
				 *              modified when calling
				 *              the app-registered
				 *              IVIDDEC3_DynamicParams.putBufferFxn().
				 *
				 *   @remarks The app/framework can
				 *              use this handle to
				 *              differentiate
				 *              callbacks from
				 *              different algorithms.
				 */
	int32_t late_acquire_arg;  /**< Argument used during late acquire.
				    *
				    *   @remarks   For all control() commands
				    *              other than
				    *              #XDM_SETLATEACQUIREARG, this
				    *              field is ignored and can
				    *              therefore be set by the
				    *              caller to any value.
				    *
				    *   @remarks   This field is used to
				    *              provide the
				    *              'late acquire' arg required by
				    *              #XDM_SETLATEACQUIREARG.
				    *
				    *   @remarks   Late acquire support is
				    *              an optional feature for
				    *              video decoders.  If the
				    *              codec supports late
				    *              acquisition of resources,
				    *              and the application has supplied
				    *              a lateAcquireArg value (via
				    *              #XDM_SETLATEACQUIREARG), then the
				    *              codec must also provide this
				    *              @c lateAcquireArg value when
				    *              requesting resources (i.e.
				    *              during their call to
				    *              acquire() when requesting
				    *              the resource).
				    */
};

bool
viddec_set_dynparams(struct codec_engine *ce,
		     int32_t size,
		     void **data)
{
	struct viddec_dynparams params = {
		.size = size,
		.decode_header = 0,   /* decode entire access unit */
		.display_width = 0,
		.frame_skip_mode = 0, /* do not skip any frame types */
		.new_frame_flag = 1,  /* true */
	};

	if (!dce_buffer_alloc(ce, size, data))
		return false;

	memcpy(*data, &params, sizeof(params));
	return true;
}

/**
 *  Single buffer descriptor.
 */
struct single_buf_desc {
    int8_t   *buf;       /**< Pointer to a buffer address. */
    int32_t  buf_size;    /**< Size of @c buf in 8-bit bytes. */
    int32_t  access_mask; /**< Mask filled by the algorithm, declaring
			   *   how the buffer was accessed <b>by the
			   *   algorithm processor</b>.
			   *
			   *   @remarks  If the buffer was <b>not</b>
			   *             accessed by the algorithm
			   *             processor (e.g., it was filled
			   *             via DMA or other hardware
			   *             accelerator that <i>doesn't</i>
			   *             write through the algorithm's
			   *             CPU), then no bits in this mask
			   *             should be set.
			   *
			   *   @remarks  It is acceptible (and
			   *             appropriate!)to set several
			   *             bits in this mask if the
			   *             algorithm accessed the buffer
			   *             in several ways.
			   *
			   *   @remarks  This mask is often used by the
			   *             application and/or framework
			   *             to appropriately manage cache
			   *             on cache-based systems.
			   *
			   *   @sa XDM_AccessMode
			   */
};

/**
 * Buffer information descriptor for input and output buffers.
 */
struct alg_buf_info {
	int32_t min_num_in_bufs;       /**< Minimum number of input buffers. */
	int32_t min_num_out_bufs;      /**< Minimum number of output buffers. */
	union buf_size min_in_buf_size[16];  /**< Minimum size required
					      * for each input buffer.
					      */
	union buf_size min_out_buf_size[16]; /**< Minimum size required
					      * for each output buffer.
					      */
	int32_t in_buf_memory_type[16]; /**< Required memory type
					 * for each input buffer.
					 *
					 *   @sa XDM_MemoryType
					 */
	int32_t out_buf_memory_type[16]; /**< Required memory type
					  * for each output buffer.
					  *
					  *   @sa XDM_MemoryType
					  */
	int32_t min_num_buf_sets;      /**< Minimum number of buffer sets for
					*   buffer management.
					*
					*   @todo  need more details
					*/
};

/**
 * Defines instance status parameters.
 */
struct viddec_status {
	int32_t size;            /**< @sizeField */
	int32_t extended_error;   /**< @extendedErrorField */
	struct single_buf_desc data;    /**< Buffer descriptor for data passing.
					 *
					 *   @remarks   If this field is not used,
					 *              the application <b>must</b>
					 *              set @c data.buf to NULL.
					 *
					 *   @remarks   This buffer can be used as
					 *              either input or output,
					 *              depending on the command.
					 *
					 *   @remarks   The buffer will be provided
					 *              by the application, and
					 *              returned to the application
					 *              upon return of the
					 *              IVIDDEC3_Fxns.control()
					 *              call.  The algorithm must
					 *              not retain a pointer to this
					 *              data.
					 *
					 *   @sa #XDM_GETVERSION
					 */
	int32_t max_num_display_bufs;/**< The maximum number of buffers that will
				      *   be required by the codec.
				      *
				      *   @remarks   The maximum number of buffers
				      *              can be IVIDEO2_MAX_IO_BUFFERS.
				      */
	int32_t max_out_args_display_bufs;/**< The maximum number of display
					   *   buffers that can be returned via
					   *   IVIDDEC3_OutArgs.displayBufs.
					   *
					   *   @remarks   If returning display buffers
					   *              embedded into the OutArgs
					   *              struct, this field provides
					   *              the size of the
					   *              OutArgs.displayBufs.bufDesc[]
					   *              array.
					   */
	int32_t output_height;    /**< Output height in pixels. */
	int32_t output_width;     /**< Output width in pixels. */
	int32_t framerate;       /**< Average frame rate in fps * 1000.
				  *   For example, if average frame rate is 30
				  *   frames per second, this field should be
				  *   30000.
				  */
	int32_t bitrate;         /**< Average bit rate, in bits per second. */
	int32_t content_type;     /**< @copydoc IVIDEO_ContentType
				   *
				   *   @sa IVIDEO_ContentType
				   */
	int32_t sample_aspect_ratio_height;/**< Sample aspect ratio height. */
	int32_t sample_aspect_ratio_width;/**< Sample aspect ratio width. */
	int32_t bit_range;        /**< Full 8 bit, CCIR 601 */
	int32_t force_chroma_format;/**< Output chroma format.
				     *
				     *   @sa    XDM_ChromaFormat
				     */
	int32_t operating_mode;   /**< Video decoding mode of operation.
				   *
				   *   @sa IVIDEO_OperatingMode
				   */
	int32_t frame_order;      /**< Frame Order
				   *
				   *   @remarks   This field reflects the value
				   *              provided during creation in
				   *              IVIDDEC3_Params.displayDelay
				   */
	int32_t input_data_mode;   /**< Input data mode.
				    *
				    *   @sa IVIDDEC3_Params.inputDataMode
				    *   @sa IVIDEO_DataMode
				    */
	int32_t output_data_mode;  /**< Output data mode.
				    *
				    *   @sa IVIDDEC3_Params.outputDataMode
				    *   @sa IVIDEO_DataMode
				    */
	struct alg_buf_info buf_info;    /**< Input and output buffer information.
					  *
					  *   @remarks This field
					  *              provides the
					  *              application
					  *              with the
					  *              algorithm's
					  *              buffer
					  *              requirements.
					  *              The
					  *              requirements
					  *              may vary
					  *              depending on
					  *              the current
					  *              configuration
					  *              of the
					  *              algorithm
					  *              instance.
					  *
					  *   @sa XDM1_AlgBufInfo
					  */
	int32_t num_input_data_units;/**< Number of input slices/rows.
				      *
				      *   @remarks   Units depend on the
				      *              IVIDDEC3_Params.inputDataMode,
				      *              like number of
				      *              slices/rows/blocks etc.
				      *
				      *   @remarks   Ignored if
				      *              IVIDDEC3_Params.inputDataMode
				      *              is set to full frame mode.
				      *
				      *   @sa IVIDDEC3_Params.inputDataMode
				      */
	int32_t num_output_data_units;/**< Number of output slices/rows.
				       *
				       *   @remarks  Units depend on the
				       *             @c outputDataMode, like number of
				       *             slices/rows/blocks etc.
				       *
				       *   @remarks   Ignored if
				       *              IVIDDEC3_Params.outputDataMode
				       *              is set to full frame mode.
				       *
				       *   @sa IVIDDEC3_Params.outputDataMode
				       */
	int32_t configuration_id; /**< Configuration ID of given codec.
				   *
				   *   @remarks   This is based on the input
				   *              stream & can be used by the
				   *              framework to optimize the
				   *              save/restore overhead of any
				   *              resources used.
				   *
				   *   @remarks   This can be useful in
				   *              multichannel use case
				   *              scenarios.
				   */
	int32_t metadata_type[3];/**< Type of each metadata plane.
				  *
				  *   @sa IVIDEO_MetadataType
				  */
	struct viddec_dynparams dec_dynamic_params;/**< Current values
						    *   of the
						    *   decoder's
						    *   dynamic
						    *   parameters.
						    *
						    *   @remarks This
						    *              is
						    *              the
						    *              last
						    *              field
						    *              in
						    *              the
						    *              base
						    *              struct
						    *              as
						    *              it
						    *              can
						    *              be
						    *              extended.
						    */
};

bool
viddec_set_status(struct codec_engine *ce,
		  int32_t size,
		  void **data)
{
	struct viddec_status params = {
		.size = size,
	};

	if (!dce_buffer_alloc(ce, size, data))
		return false;

	memcpy(*data, &params, sizeof(params));
	return true;
}

int32_t
viddec_status_get_error(void *data)
{
	struct viddec_status *status;
	status = data;
	return status->extended_error;
}

bool
viddec_set_buffer_desc(struct codec_engine *ce,
		       int32_t num,
		       struct single_buf_desc_2 descs[],
		       void **buffer)
{
	struct buf_desc params = {
		.num_bufs = num,
	};
	memcpy(&params.descs, descs, num * sizeof(descs[0]));

	size_t size = sizeof(struct buf_desc);
	if (!dce_buffer_alloc(ce, size, buffer))
		return false;

	memcpy(*buffer, &params, size);
	return true;
}

bool
viddec_set_in_args(struct codec_engine *ce,
		   int32_t size,
		   void **data)
{
	struct viddec_in_args params = {
		.size = size,
	};

	if (!dce_buffer_alloc(ce, size, data))
		return false;

	memcpy(*data, &params, sizeof(params));
	return true;
}


bool
viddec_set_out_args(struct codec_engine *ce,
		    int32_t size,
		    void **data)
{
	struct viddec_out_args params = {
		.size = size,
	};

	if (!dce_buffer_alloc(ce, size, data))
		return false;

	memcpy(*data, &params, sizeof(params));
	return true;
}

struct h264vdec_status {
	struct viddec_status status;
	int32_t reserved[7];
	uint32_t gap_in_frame_num;
	uint32_t sps_max_ref_frames;
};

bool
h264vdec_set_status(struct codec_engine *ce,
		    void **data)
{
	return viddec_set_status(ce,
				 sizeof(struct h264vdec_status),
				 data);
}

struct h264vdec_params {
	struct viddec_params params;
	int32_t dpb_size_in_frames;      /* new name, same meaning */
	int32_t p_constant_memory;
	int32_t bit_stream_format;
	uint32_t err_concealment_mode;
	int32_t temporal_dir_mode_pred;
	int32_t reserved_1[4];
	int32_t preset_level_idc;
	int32_t preset_profile_idc;
	uint32_t detect_cabac_align_err;
	uint32_t detect_ipcm_align_err;
	int32_t reserved_2[5];
};

bool
h264vdec_set_params(struct codec_engine *ce,
		    int32_t height,
		    int32_t width,
		    void **data)
{
	struct h264vdec_params *p;

	if (!viddec_set_params(ce,
			       height,
			       width,
			       sizeof(*p),
			       data))
		return false;

	p = (struct h264vdec_params *) *data;

	p->dpb_size_in_frames = -1;    /* auto */
	p->p_constant_memory = 0;
	p->preset_level_idc = 12;      /* level 41 */
	p->err_concealment_mode = 1;   /* apply concealment */
	p->temporal_dir_mode_pred = 1; /* true */

	return true;
}

struct h264vdec_dynparams {
	struct viddec_dynparams dynparams;
	int32_t deblock_filter_mode;
	int32_t reserved[7];
};

bool
h264vdec_set_dynparams(struct codec_engine *ce,
		       void **data)
{
	return viddec_set_dynparams(ce,
				    sizeof(struct h264vdec_dynparams),
				    data);
}

bool
h264vdec_set_in_args(struct codec_engine *ce,
		     void **data)
{
	return viddec_set_in_args(ce,
				  sizeof(struct viddec_in_args),
				  data);
}

bool
h264vdec_set_out_args(struct codec_engine *ce,
		      void **data)
{
	return viddec_set_out_args(ce,
				   sizeof(struct viddec_out_args),
				   data);
}
