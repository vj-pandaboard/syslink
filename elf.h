/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#ifndef ELF_H
#define ELF_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

typedef void(*segment_func)(FILE *,
			    uint32_t,
			    uint32_t,
			    uint32_t,
			    uint32_t,
			    void *b);

bool get_elf_info(const char *filename,
		  uint32_t *entry,
		  uint32_t *start,
		  segment_func func,
		  void *cont);

#endif /* ELF_H */
