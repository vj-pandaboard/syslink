/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#ifndef SYSLINK_H
#define SYSLINK_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h> /* for FILE */
#include <pthread.h>

struct sharedregion_config {
	size_t cache_size;
	uint16_t num_entries;
	bool translate;
};

struct sharedregion_entry {
	uint32_t base;
	size_t len;
	uint16_t owner;
	bool is_valid;
	bool cache_enable;
	size_t cache_size;
	bool create_heap;
	char *name;
};

struct sharedregion_region {
	struct sharedregion_entry entry;
	size_t size;
	void *handle;
};

struct messageq_msg {
	uint32_t reserved0;
	uint32_t reserved1;
	uint32_t size;
	uint16_t flags;
	uint16_t id;
	uint16_t dst_id;
	uint16_t dst_proc;
	uint16_t reply_id;
	uint16_t reply_proc;
	uint16_t src_proc;
	uint16_t heap_id;
	uint16_t seq_num;
	uint16_t reserved;
};

struct heapbufmp_params {
	const char *name;
	uint16_t region_id;
	uint32_t pa;
	uint32_t block_size;
	uint32_t num_blocks;
	uint32_t align;
	bool exact;
	void *gate;
};

enum procmgr_boot_mode {
	BOOT, NOLOAD, NOBOOT, END,
};

enum procmgr_proc_type {
	TESLA, APPM3, SYSM3, MPU,
};

enum procmgr_event_type {
	MMU_FAULT, ERROR, STOP, START, WATCHDOG,
};

enum deh_event_type {
	SYS_ERROR = 1, WATCHDOG_ERROR,
};

enum procmgr_proc_state {
	STATE_UNKNOWN,
	STATE_POWERED,
	STATE_RESET,
	STATE_LOADED,
	STATE_RUNNING,
	STATE_UNAVAILABLE,
	STATE_LOADING,
};

struct procmgr_addr_info {
	bool is_init;
	uint32_t addr[3];
	uint32_t size;
};

struct procmgr_proc_info {
	enum procmgr_boot_mode mode;
	uint16_t numentries;
	struct procmgr_addr_info entries[32];
};

int s_ipc_open(void);
int s_proc_open(void);

int s_proc_tesla_open(void);
int s_proc_sysm3_open(void);
int s_proc_appm3_open(void);

int s_dmm_mpu_open(void);
int s_dmm_tesla_open(void);

int s_deh_tesla_open(void);
int s_deh_sysm3_open(void);
int s_deh_appm3_open(void);

bool s_sharedregion_get_config(int handle, struct sharedregion_config *cfg);
bool s_sharedregion_get_region_info(int handle,
				    struct sharedregion_region *regions);
bool s_sharedregion_get_heap(int handle,
			     uint16_t id,
			     void **heap);
bool s_sharedregion_get_pa(uint32_t sr_addr,
			   uint32_t num_entries,
			   struct sharedregion_region *regions,
			   uintptr_t *pa);
bool s_sharedregion_get_id(uint32_t pa,
			   uint32_t num_entries,
			   struct sharedregion_region *regions,
			   uint16_t *id);
bool s_sharedregion_get_sr_addr(uint32_t pa,
				uint16_t id,
				uint32_t num_entries,
				struct sharedregion_region *regions,
				uint32_t *sr_addr);

bool s_messageq_get_sync(int handle, void **sync);
bool s_messageq_register_heap(int handle, uint16_t id, void *heap);
bool s_messageq_unregister_heap(int handle, uint16_t id);
bool s_messageq_create(int handle,
		       char *name,
		       void **sync,
		       void **queue,
		       uint32_t *id);
bool s_messageq_delete(int handle, void *queue);
bool s_messageq_open(int handle,
		     char *name,
		     uint32_t *qid);
bool s_messageq_close(int handle,
		      uint32_t qid);
bool s_messageq_alloc(int handle,
		      uint16_t heap_id,
		      uint32_t size,
		      uint32_t *sr_addr);
bool s_messageq_free(int handle,
		     uint32_t sr_addr);
static inline void s_messageq_set_reply_queue(uint32_t qid,
					      struct messageq_msg *msg)
{
	msg->reply_id = qid;
	msg->reply_proc = qid >> 16;
}
bool s_messageq_put(int handle,
		    uint32_t qid,
		    uint32_t sr_addr);
int s_messageq_get(int handle,
		   void *queue,
		   uint32_t timeout,
		   uint32_t *sr_addr);

bool s_notify_thread_attach(int handle, pthread_t *tid);
bool s_notify_thread_detach(int handle, pthread_t tid);

bool s_procmgr_open(int handle, uint16_t id, void **ret_proc);
bool s_procmgr_close(int handle, void *proc);
bool s_procmgr_get_attach_params(int handle,
				 void *proc,
				 enum procmgr_boot_mode *bootmode);
bool s_procmgr_attach(int handle,
		      void *proc,
		      enum procmgr_boot_mode *bootmode,
		      struct procmgr_proc_info *info);
bool s_procmgr_map_memory_region(int handle,
				 struct procmgr_proc_info *info);
bool s_procmgr_unmap_memory_region(struct procmgr_proc_info *info);
bool s_procmgr_detach(int handle, void *proc);
bool s_procmgr_get_state(int handle,
			 void *proc,
			 enum procmgr_proc_state *state);
bool s_procmgr_load_elf_segment(int handle,
				FILE *image,
				uint32_t vaddr,
				size_t memsz,
				size_t filesz,
				long int offset);
bool s_procmgr_map_sharedregions(int hande,
				 uint16_t num_entries,
				 struct sharedregion_region regions[]);
bool s_procmgr_unmap_sharedregions(uint16_t num_entries,
				   struct sharedregion_region regions[]);

bool s_rproc_start(int handle, uint32_t addr);
bool s_rproc_stop(int handle);
bool s_rproc_register_event(int handle,
			    uint16_t id,
			    int32_t fd,
			    enum procmgr_event_type event_type);
bool s_rproc_unregister_event(int handle,
			      uint16_t id,
			      int32_t fd,
			      enum procmgr_event_type event_type);

bool s_heapbufmp_params_init(int handle,
			     struct heapbufmp_params *params);
bool s_heapbufmp_get_sharedmem_requested_size(int handle,
					      struct heapbufmp_params *params,
					      uint32_t *size);
bool s_heapbufmp_create(int handle,
			struct heapbufmp_params *params,
			uint32_t sr_addr,
			void **heap);
bool s_heapbufmp_delete(int handle, void *heap);

bool s_heapmemmp_alloc(int handle,
		       void *heap,
		       uint32_t size,
		       uint32_t align,
		       uint32_t *sr_addr);
bool s_heapmemmp_free(int handle,
		      void *heap,
		      uint32_t size,
		      uint32_t sr_addr);

enum ipc_control_cmd {
	LOADCALLBACK = 0xbabe0000,
	STARTCALLBACK,
	STOPCALLBACK
};

bool s_ipc_setup(int handle);
bool s_ipc_destroy(int handle);
bool s_ipc_control(int handle, uint16_t id, int32_t cmd, uint32_t arg);

bool s_dmm_mpu_init(int handle);
bool s_dmm_create_pool(int handle,
		       uint32_t pool_id,
		       uint32_t da_begin,
		       uint32_t da_end,
		       uint32_t size,
		       uint32_t flags);
bool s_dmm_delete_pool(int handle, uint32_t pool_id);
bool s_mmu_register_event(int handle, int fd);
bool s_mmu_unregister_event(int handle, int fd);

bool s_deh_register_event(int handle,
			  enum deh_event_type event_type,
			  int fd);
bool s_deh_unregister_event(int handle,
			    enum deh_event_type event_type,
			    int fd);

#endif /* SYSLINK_H */
