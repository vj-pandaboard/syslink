/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#include "syslink.h"
#include "elf.h"
#include "log.h"
#include "util.h"

#include <unistd.h> /* close */
#include <stdio.h> /* perror */
#include <signal.h> /* signal */
#include <string.h> /* strcmp */
#include <errno.h>
#include <sys/eventfd.h>

bool daemon_FLAG = false;

/* global configuration */
struct sharedregion_config sr_cfg;

/* heaps to allocate */
struct heap_data {
	const char *name;
	uint32_t block_size;
	uint32_t num_blocks;
	uint32_t align;
	uint16_t heap_id;
	void *heap;
	uint32_t size;
	uint32_t sr_addr;
} heap_data[] = {
	{ "Heap0", 256, 128, 128, 0, NULL, 0, 0, }, /* TILER */
	{ "Heap1", 384, 256, 128, 1, NULL, 0, 0, }, /* DOMX/DCE */
};

static inline bool
setup(int handle,
      pthread_t *tid)
{
	if (!s_sharedregion_get_config(handle, &sr_cfg)) {
		pr_err("sharedregion failed!: %s", strerror(errno));
		return false;
	}

	if (!s_ipc_setup(handle)) {
		pr_err("ipc failed!: %s", strerror(errno));
		return false;
	}

	if (!s_notify_thread_attach(handle, tid)) {
		pr_err("notify failed!: %s", strerror(errno));
		return false;
	}

	return true;
}

static inline bool
destroy(int handle)
{
	return s_ipc_destroy(handle);
}

static inline bool
proc_init(int handle, int id, void **proc,
	  struct procmgr_proc_info *info)
{
	*proc = NULL;
	if (!s_procmgr_open(handle, id, proc))
		return false;

	enum procmgr_boot_mode bootmode;
	if (!s_procmgr_get_attach_params(handle,
					 NULL,
					 &bootmode))
		return false;

	if (!s_procmgr_attach(handle, *proc, &bootmode, info))
		return false;

	if (!s_procmgr_map_memory_region(handle, info))
		return false;

	return true;
}

static inline bool
proc_deinit(int handle, void *proc,
	    struct procmgr_proc_info *info)
{
	bool res;

	res = s_procmgr_detach(handle, proc);
	res &= s_procmgr_unmap_memory_region(info);
	res &= s_procmgr_close(handle, proc);

	return res;
}

static inline bool
proc_start(int ipc_handle,
	   int rproc_handle,
	   uint16_t id,
	   uint32_t entry,
	   uint32_t start)
{
	/*  @TODO: hack for tesla */

	if (!s_ipc_control(ipc_handle, id, LOADCALLBACK, start))
		return false;

	if (!s_rproc_start(rproc_handle, entry))
		return false;

	if (!s_ipc_control(ipc_handle, id, STARTCALLBACK, 0))
		return false;

	return true;
}

static void
process_elf_segment(FILE *f,
		    uint32_t vaddr,
		    uint32_t memsz,
		    uint32_t filesz,
		    uint32_t offset,
		    void *cont)
{
	int handle = (int) (long) cont;
	s_procmgr_load_elf_segment(handle, f, vaddr, memsz, filesz, offset);
}

static inline bool
proc_load_and_start(int ipc_handle,
		    int proc_handle,
		    int rproc_handle,
		    uint16_t id,
		    const char *file)
{
	uint32_t start, entry;
	void *cont = (void *) (long) proc_handle;

	start = entry = 0;

	if (!get_elf_info(file,
			 &entry,
			 &start,
			 process_elf_segment,
			 cont))
		return false;

	if (!proc_start(ipc_handle,
			rproc_handle,
			id,
			entry,
			start))
		return false;

	return true;
}

static inline bool
proc_stop(int ipc_handle,
	  int rproc_handle,
	  uint16_t id)
{
	if (!s_ipc_control(ipc_handle, id, STOPCALLBACK, 0))
		return false;

	if (!s_rproc_stop(rproc_handle))
		return false;

	return true;
}

static inline struct sharedregion_region *
get_regions(int ipc_handle,
	    int proc_handle,
	    uint16_t num_entries)
{
	struct sharedregion_region *regions = NULL;

	regions = calloc(num_entries,
			 sizeof(struct sharedregion_region));

	if (!s_sharedregion_get_region_info(ipc_handle,
					    regions))
		goto bail;

	if (!s_procmgr_map_sharedregions(proc_handle,
					 num_entries,
					 regions))
		goto unmap_and_bail;

	return regions;

unmap_and_bail:
	s_procmgr_unmap_sharedregions(num_entries, regions);

bail:
	free(regions);
	return NULL;
}

static inline bool
unregister_heap_in_msgq(int handle,
			uint16_t id,
			void *heap,
			void *sr_heap,
			uint32_t size,
			uint32_t sr_addr)
{
	bool ret;
	ret = s_messageq_unregister_heap(handle, id);
	ret &= s_heapbufmp_delete(handle, heap);
	ret &= s_heapmemmp_free(handle, sr_heap, size, sr_addr);
	return ret;
}

static inline bool
register_heap_in_msgq(int handle,
		      const char *name,
		      uint32_t block_size,
		      uint32_t num_blocks,
		      uint32_t align,
		      uint16_t heap_id,
		      void *sr_heap,
		      struct sharedregion_region regions[],
		      void **heap,
		      uint32_t *size,
		      uint32_t *sr_addr)
{
	struct heapbufmp_params params;

	if (!s_heapbufmp_params_init(handle, &params))
		return false;

	params.block_size = block_size;
	params.num_blocks = num_blocks;
	params.align = align;
	params.name = name;

	if (!s_heapbufmp_get_sharedmem_requested_size(handle,
						      &params,
						      size))
		return false;

	if (!s_heapmemmp_alloc(handle,
			       sr_heap,
			       *size,
			       4, /* default alignment */
			       sr_addr))
		return false;

	if (*sr_addr == (uint32_t) ~0) /* is it invalid? */
		return false;

	uintptr_t pa = 0;
	if (!s_sharedregion_get_pa(*sr_addr,
				   sr_cfg.num_entries,
				   regions,
				   &pa))
		return false;

	params.pa = pa;

	if (!s_heapbufmp_create(handle, &params, *sr_addr, heap))
		return false;

	if (!s_messageq_register_heap(handle, heap_id, *heap))
		goto bail;

	return true;

bail:
	unregister_heap_in_msgq(handle,
				heap_id,
				*heap,
				sr_heap,
				*size,
				*sr_addr);

	return false;
}

static bool
event_loop(int mmu_handle)
{
	bool ret = false;
	int appm3_deh_handle, sysm3_deh_handle;
	appm3_deh_handle = sysm3_deh_handle = -1;

	sysm3_deh_handle = s_deh_sysm3_open();
	if (sysm3_deh_handle == -1)
		return false;

	appm3_deh_handle = s_deh_appm3_open();
	if (appm3_deh_handle == -1)
		goto bail;

	int efd[5];

	for (int i = 0; i < 5; i++)
		if ((efd[i] = eventfd(0, 0)) == -1)
			goto bail;

	if (!s_mmu_register_event(mmu_handle, efd[0]))
		goto close_and_bail;
	if (!s_deh_register_event(sysm3_deh_handle,
				  SYS_ERROR,
				  efd[1]))
		goto close_and_bail;
	if (!s_deh_register_event(sysm3_deh_handle,
				  WATCHDOG_ERROR,
				  efd[2]))
		goto close_and_bail;
	if (!s_deh_register_event(appm3_deh_handle,
				  SYS_ERROR,
				  efd[3]))
		goto close_and_bail;
	if (!s_deh_register_event(appm3_deh_handle,
				  WATCHDOG_ERROR,
				  efd[4]))
		goto close_and_bail;

	if (daemon_FLAG) {
		fd_set fds;
		FD_ZERO(&fds);
		for (int i; i < 5; i++)
			FD_SET(efd[i], &fds);

		ret = select(6, &fds, NULL, NULL, NULL) > 0;
		if(!ret)
			pr_warning("event signaled!: %s", strerror(errno));
	}

	ret &= s_mmu_unregister_event(mmu_handle, efd[0]);
	ret &= s_deh_unregister_event(sysm3_deh_handle,
				      SYS_ERROR,
				      efd[1]);
	ret &= s_deh_unregister_event(sysm3_deh_handle,
				      WATCHDOG_ERROR,
				      efd[2]);
	ret &= s_deh_unregister_event(appm3_deh_handle,
				      SYS_ERROR,
				      efd[3]);
	ret &= s_deh_unregister_event(appm3_deh_handle,
				      WATCHDOG_ERROR,
				      efd[4]);

close_and_bail:
	for (int i = 0; i < 5; i++)
		close(efd[i]);

bail:
	if (sysm3_deh_handle != -1)
		close(sysm3_deh_handle);
	if (appm3_deh_handle != -1)
		close(appm3_deh_handle);

	return ret;
}

static bool
process_flags(int *argc, char **argv)
{
	for (int i = 1; i < *argc;) {
		int j = i;
		const char* arg = argv[i++];
		if (arg[0] != '-')
			break;
		if (strcmp(arg, "-d") == 0) {
			daemon_FLAG = true;
			while (j < i) { argv[j++] = NULL; }
		} else {
			pr_err("Unknown argument");
			return false;
		}
	}

	int j = 1;
	for (int i = 1; i < *argc; i++) {
		if (argv[i] != NULL)
			argv[j++] = argv[i];
	}
	*argc = j;

	return true;
}

static void
termination_handler(int signum)
{
	pr_debug("got a signal: %s", strsignal(signum));
	daemon_FLAG = false;
}

int
main(int argc, char **argv)
{
	int ret = EXIT_FAILURE;
	void *sysm3_proc, *appm3_proc;
	int appm3_handle, sysm3_handle;
	int mpu_handle = -1;
	struct procmgr_proc_info sysm3_info, appm3_info;
	pthread_t tid;
	bool thread_init = false;

	sysm3_proc = appm3_proc = NULL;
	appm3_handle = sysm3_handle = -1;

	if (!process_flags(&argc, argv))
		return EXIT_FAILURE;

	{
		signal(SIGINT, termination_handler);
		signal(SIGTERM, termination_handler);
	}


	int ipc_handle = s_ipc_open();
	if (ipc_handle == -1)
		return EXIT_FAILURE;

	int proc_handle = s_proc_open();
	if (proc_handle == -1)
		goto bail;

	if (!setup(ipc_handle, &tid))
		goto bail;
	thread_init = true;

	/* open remote processors */
	{
		if (!proc_init(proc_handle, SYSM3, &sysm3_proc, &sysm3_info))
			goto bail;

		if (!proc_init(proc_handle, APPM3, &appm3_proc, &appm3_info))
			goto bail;
	}

	/* open MMU processors' IO */
	{
		/* only m3 for now */
		mpu_handle = s_dmm_mpu_open();
		if (mpu_handle == -1)
			goto bail;
		if (!s_dmm_mpu_init(mpu_handle))
			goto bail;
		if (!s_dmm_create_pool(mpu_handle,
				       0,                       /* pool id  */
				       0x10000000,              /* size     */
				       0x90000000,              /* da begin */
				       0x90000000 + 0x10000000, /* da end   */
				       0                        /* flags    */ ))
			goto bail;
	}

	/* load and start remote processors */
	{
		sysm3_handle = s_proc_sysm3_open();
		if (sysm3_handle == -1)
			goto bail;

		if (!proc_load_and_start(ipc_handle,
					 proc_handle,
					 sysm3_handle,
					 SYSM3,
					 argv[1]))
			goto bail;

		appm3_handle = s_proc_appm3_open();
		if (appm3_handle == -1)
			goto bail;

		if (!proc_load_and_start(ipc_handle,
					 proc_handle,
					 appm3_handle,
					 APPM3,
					 argv[2]))
			goto bail;
	}

        /* create and map the shared regions */
	struct sharedregion_region *regions;
	regions = get_regions(ipc_handle,
			      proc_handle,
			      sr_cfg.num_entries);
	if (!regions)
		goto stop_and_bail;

	void *sr_heap = NULL;
	{
		if (!s_sharedregion_get_heap(ipc_handle,
					     1, /* RCM messageq heap */
					     &sr_heap))
			goto stop_and_bail;

		if (!sr_heap)
			goto stop_and_bail;

		for (uint i = 0; i < ARRAY_SIZE(heap_data); i++) {
			struct heap_data *d = &heap_data[i];
			if (!register_heap_in_msgq(ipc_handle,
						   d->name,
						   d->block_size, /* tiler heap block size */
						   d->num_blocks, /* tiler heap num blocks */
						   d->align,      /* tiler heap align */
						   d->heap_id,    /* tiler heap id */
						   sr_heap,
						   regions,
						   &d->heap,
						   &d->size,
						   &d->sr_addr))
				goto stop_and_bail;
		}
	}

	event_loop(mpu_handle);

	for (uint i = 0; i < ARRAY_SIZE(heap_data); i++) {
		struct heap_data *d = &heap_data[i];
		if (!unregister_heap_in_msgq(ipc_handle,
					     d->heap_id, /* tiler heap id */
					     d->heap,
					     sr_heap,
					     d->size,
					     d->sr_addr))
			goto stop_and_bail;
	}

	s_procmgr_unmap_sharedregions(sr_cfg.num_entries,
				      regions);

	ret = EXIT_SUCCESS;

stop_and_bail:
	free(regions);

	/* stop remote processors */
	{
		if (!proc_stop(ipc_handle,
			       appm3_handle,
			       APPM3))
			ret = EXIT_FAILURE;

		if (!proc_stop(ipc_handle,
			       sysm3_handle,
			       SYSM3))
			ret = EXIT_FAILURE;
	}

bail:
	{
		if (mpu_handle != -1) {
			if (!s_dmm_delete_pool(mpu_handle, 0))
				ret = EXIT_FAILURE;

			close(mpu_handle);
		}
	}

	if (sysm3_proc)
		if (!proc_deinit(proc_handle, sysm3_proc, &sysm3_info))
			ret = EXIT_FAILURE;

	if (appm3_proc)
		if (!proc_deinit(proc_handle, appm3_proc, &appm3_info))
			ret = EXIT_FAILURE;

	if (sysm3_handle != -1)
		close(sysm3_handle);

	if (appm3_handle != -1)
		close(appm3_handle);

	if (proc_handle != -1)
		close(proc_handle);

	if (thread_init)
		s_notify_thread_detach(ipc_handle, tid);

	destroy(ipc_handle);

	close(ipc_handle);

	return ret;
}
