/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

/* Tiled memory manager */

#include "tiler.h"

#include <fcntl.h> /* open */
#include <unistd.h> /* close */
#include <sys/ioctl.h> /* ioctl */
#include <sys/mman.h> /* mmap */
#include <string.h> /* memcpy */

#include "util.h"

#define TILER 0x7A

#define TILER_GETBLK     _IOWR(TILER, 100, struct tiler_block_info)
#define TILER_FREEBLK    _IOW(TILER, 101, struct tiler_block_info)
#define TILER_GETPHYADDR _IOWR(TILER, 102, uint32_t)
#define TILER_REGBUF     _IOWR(TILER, 106, struct tiler_buffer_info)
#define TILER_UNREGBUF   _IOWR(TILER, 107, struct tiler_buffer_info)

int
tiler_open()
{
	return open("/dev/tiler", O_SYNC | O_RDWR);
}

static bool
get_block(int handle,
	  struct tiler_block_info *block)
{
	if (ioctl(handle, TILER_GETBLK, block) < 0)
		return false;

	return true;
}

static bool
free_block(int handle,
	   struct tiler_block_info *block)
{
	if (ioctl(handle, TILER_FREEBLK, block) < 0)
		return false;

	return true;
}


static bool
unregister_buffer(int handle,
		  struct tiler_buffer_info *buffer)
{
	if (ioctl(handle, TILER_UNREGBUF, buffer) < 0)
		return false;

	return true;
}

static bool
register_buffer(int handle,
		struct tiler_buffer_info *buffer)
{
	if (ioctl(handle, TILER_REGBUF, buffer) < 0)
		return false;

	return true;
}

static bool
tiler_mmap(int handle,
	   uint32_t off,
	   uint32_t len,
	   void **base)
{
	off_t addr = MOD(off);
	size_t length = POWERUP(len + addr);
	off_t offset = POWERDOWN(off);

	*base = mmap(NULL, length,
		     PROT_READ | PROT_WRITE, MAP_SHARED,
		     handle, offset);

	if (*base == MAP_FAILED)
		return false;

	*base += addr;

	return true;
}

static bool
tiler_munmap(void *base,
	     uint32_t off,
	     uint32_t len)
{
	void *ptr = (void *) POWERDOWN((uintptr_t) base);
	off_t addr = MOD(off);
	size_t length = POWERUP(len + addr);

	if (munmap(ptr, length) < 0)
		return false;

	return true;
}

static inline size_t
get_bpp(enum tiler_fmt fmt)
{
	if (fmt == TILFMT_32BIT)
		return 4;
	else if (fmt == TILFMT_16BIT)
		return 2;

	return 1;
}

struct area {
	uint16_t w, h;
};

static inline size_t
get_size(struct tiler_block_info *block)
{
	size_t size;
	if (block->fmt == TILFMT_PAGE)
		size = POWERUP(block->offs + block->len);
	else {
		struct area a;
		memcpy(&a, &block->len, sizeof(a));
		size = a.h * POWERUP(block->offs + a.w * get_bpp(block->fmt));
	}

	return size;
}

static inline bool
alloc(int handle,
      struct tiler_buffer_info *buffer)
{
	if (!register_buffer(handle,
			     buffer))
		goto bail;

	void *base;
	if (!tiler_mmap(handle,
			buffer->off,
			buffer->len,
			&base))
		goto bail;

	size_t size = 0;
	for (uint16_t i = 0; i < buffer->num_blocks; i++) {
		buffer->blocks[i].ptr = base + size;
		size += get_size(&buffer->blocks[i]);
	}

	return true;

bail:
	unregister_buffer(handle,
			  buffer);
	return false;
}

bool
tiler_alloc(int handle,
	    struct tiler_buffer_info *buffer)
{
	/* initialize each block info and allocate each buffer */
	uint16_t c;
	for (c = 0; c < buffer->num_blocks; c++) {
		struct tiler_block_info *b;
		b = &buffer->blocks[c];
		if (c > 0) {
			/* continue offset between pages */
			b->align = PAGE_SIZE;
			b->offs = buffer->blocks[c - 1].offs;
		}

		b->ptr = NULL;
		if (!get_block(handle, b))
			goto bail;
	}

	if (!alloc(handle, buffer))
		goto bail;

	return true;

bail:
	for (int i = 0; i < c; i++)
		free_block(handle,
			   &buffer->blocks[i]);

	return false;
}

bool
tiler_get_pa(int handle,
	     void *vaddr,
	     uintptr_t *paddr)
{
	*paddr = ioctl(handle, TILER_GETPHYADDR, vaddr);

	return true;
}

bool
tiler_free(int handle,
	   struct tiler_buffer_info *buffer)
{
	bool ret;

	ret = unregister_buffer(handle, buffer);

	void *ptr = buffer->blocks[0].ptr;
	for (uint32_t i = 0; i < buffer->num_blocks; i++)
		ret &= free_block(handle,
				  &buffer->blocks[i]);

	ret &= tiler_munmap(ptr,
			    buffer->off,
			    buffer->len);

	return ret;
}
