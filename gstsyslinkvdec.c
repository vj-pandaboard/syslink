/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#include "gstsyslinkvdec.h"
#include "plugin.h"

#include "syslink.h"
#include "tiler.h"
#include "viddec.h"
#include "util.h"

#include <unistd.h> /* for close */
#include <string.h> /* for strcmp */

#define GST_CAT_DEFAULT gstsyslink_debug

static GstElementClass *parent_class;

enum {
	FLUSH_BUFFERS = 1 << 0,
	PUSH_BUFFERS = 1 << 1,
};

static void
save_codec_data(GstSyslinkVDec *self,
		GstStructure *in_struc)
{
	const GValue *codec_data;
	GstBuffer *buf;

	codec_data = gst_structure_get_value(in_struc, "codec_data");
	if (!codec_data)
		return;

	buf = gst_value_get_buffer(codec_data);
	if (!buf)
		return;

	gst_buffer_unref(self->codec_data);
	self->codec_data = gst_buffer_ref(buf);
}

static inline void
calculate_padding(GstSyslinkVDec *self)
{
	int w = self->width;
	int h = self->height;

	switch(self->alg) {
	case GSTSYSLINK_H264DEC:
		self->padded_width = ALIGN(w + 64, 7);
		self->padded_height = h + 96;
		break;
	default:
		break;
	}
}

static inline void
configure_caps(GstSyslinkVDec *self,
	       GstCaps *in,
	       GstCaps *out)
{
	GstStructure *out_struc, *in_struc;
	in_struc = gst_caps_get_structure(in, 0);

	out_struc = gst_structure_new("video/x-raw-yuv-strided",
		"format", GST_TYPE_FOURCC, GST_MAKE_FOURCC('N', 'V', '1', '2'),
		"rowstride", G_TYPE_INT, 4096,
		NULL);

	{
		int w, h;
		if (gst_structure_get_int(in_struc, "width", &w) &&
		    gst_structure_get_int(in_struc, "height", &h)) {
			self->width = ALIGN(w, 4);
			self->height = ALIGN(h, 4);
		}

		calculate_padding(self);

		gst_structure_set(out_struc, "width",
				  G_TYPE_INT, self->padded_width, NULL);
		gst_structure_set(out_struc, "height",
				  G_TYPE_INT, self->padded_height, NULL);
	}


	{
		const GValue *aspect_ratio;
		aspect_ratio = gst_structure_get_value(in_struc,
						       "pixel-aspect-ratio");
		if (aspect_ratio)
			gst_structure_set_value(out_struc,
						"pixel-aspect-ratio", aspect_ratio);
	}

	{
		const GValue *framerate = NULL;
		framerate = gst_structure_get_value(in_struc, "framerate");
		if (framerate)
			gst_structure_set_value(out_struc, "framerate", framerate);
		else
			/* FIXME this is a workaround for xvimagesink */
			gst_structure_set(out_struc, "framerate",
					  GST_TYPE_FRACTION, 0, 1, NULL);
	}

	gst_caps_append_structure(out, out_struc);
}

static gboolean
sink_setcaps(GstPad *pad,
	     GstCaps *caps)
{
	GstSyslinkVDec *self;
	self = GST_SYSLINK_VDEC(GST_PAD_PARENT(pad));

	GstStructure *in_struc;
	in_struc = gst_caps_get_structure(caps, 0);

	const char *name;
	name = gst_structure_get_name(in_struc);
	if (strcmp(name, "video/x-h264") == 0) {
		self->alg = GSTSYSLINK_H264DEC;
	}

	GstCaps *out_caps;
	out_caps = gst_caps_new_empty();
	configure_caps(self, caps, out_caps);
	self->tmp_caps = out_caps;

	gboolean ret;
	ret = gst_pad_set_caps(pad, caps);
	if (!ret)
		return FALSE;

	save_codec_data(self, in_struc);
	return TRUE;
}

struct vdec_params_setters {
	bool (*set_params)(struct codec_engine *ce,
			   int32_t height,
			   int32_t width,
			   void **data);
	bool (*set_dynparams)(struct codec_engine *ce,
			      void **data);
	bool (*set_status)(struct codec_engine *ce,
			   void **data);
	bool (*set_in_args)(struct codec_engine *ce,
			    void **data);
	bool (*set_out_args)(struct codec_engine *ce,
			     void **data);
};

struct vdec_params_setters h264vdec_params_setters = {
	.set_params = h264vdec_set_params,
	.set_dynparams = h264vdec_set_dynparams,
	.set_status = h264vdec_set_status,
	.set_in_args = h264vdec_set_in_args,
	.set_out_args = h264vdec_set_out_args,
};

static gboolean
allocate_params(GstSyslinkVDec *self,
		struct vdec_params_setters *setter)
{
	if (!setter->set_params(self->ce,
				self->height,
				self->width,
				&self->params))
		return FALSE;

	if (!setter->set_dynparams(self->ce,
				   &self->dynparams))
		return FALSE;

	if (!setter->set_status(self->ce,
				&self->status))
		return FALSE;

	if (!setter->set_in_args(self->ce,
				 &self->inargs))
		return FALSE;

	if (!setter->set_out_args(self->ce,
				  &self->outargs))
		return FALSE;

	return TRUE;
}

static void
deallocate_params(GstSyslinkVDec *self)
{
	if (self->outargs) {
		dce_buffer_free(self->ce,
				self->outargs);
		self->outargs = NULL;
	}

	if (self->inargs) {
		dce_buffer_free(self->ce,
				self->inargs);
		self->inargs = NULL;
	}

	if (self->status) {
		dce_buffer_free(self->ce,
				self->status);
		self->status = NULL;
	}

	if (self->dynparams) {
		dce_buffer_free(self->ce,
				self->dynparams);
		self->dynparams = NULL;
	}

	if (self->params) {
		dce_buffer_free(self->ce,
				self->params);
		self->params = NULL;
	}
}

static inline gboolean
allocate_inbufs(GstSyslinkVDec *self)
{
	if (!dce_buffer_alloc(self->ce,
			      self->width * self->height,
			      &self->input))
		return FALSE;

	uintptr_t buf;
	buf = dce_tile_get_pa(self->ce, self->input);

	struct single_buf_desc_2 descs[] = {
		{
			.mem_type = 0, /* raw */
			.buf = (int8_t *) buf,
		},
	};

	if (!viddec_set_buffer_desc(self->ce,
				    1,
				    descs,
				    &self->inbufs)) {
		dce_buffer_free(self->ce, self->input);
		self->input = NULL;
		return FALSE;
	}

	return TRUE;
}

static gboolean
init_codec(GstSyslinkVDec *self)
{
	if (!gst_pad_set_caps(self->srcpad, self->tmp_caps)) {
		GST_ERROR_OBJECT(self, "couldn't setup output caps");
		return FALSE;
	}

	const char *name;
	struct vdec_params_setters *allocator;

	switch (self->alg) {
	case GSTSYSLINK_H264DEC:
		name = "ivahd_h264dec";
		allocator = &h264vdec_params_setters;
		break;

	default:
		return FALSE;
	};

	if (!allocate_params(self,
			     allocator))
		goto bail;

	if (!dce_viddec_create(self->ce,
			       name,
			       self->params,
			       &self->codec))
		goto bail;

	if (!dce_viddec_control(self->ce,
				self->codec,
				1, /* set run time dynamic parameters. */
				self->dynparams,
				self->status))
		goto bail;

	if (!allocate_inbufs(self))
		goto bail;

	self->send_crop_event = TRUE;

	return TRUE;

bail:
	deallocate_params(self);
	return FALSE;
}

static struct list_head *pool;
struct buffer_pool {
	void *data;
	struct list_head list;
};

static void
pool_free(GstSyslinkVDec *self)
{
	struct buffer_pool *item, *tmp;
	void *data;

	list_for_each_entry_safe(item,
				 tmp,
				 pool,
				 list) {
		data = item->data;
		dce_buffer_free(self->ce, data);
		list_del(&item->list);
		free(item);
	}
}

static void
pool_push(struct buffer_pool *item)
{
	list_add_tail(&item->list, pool);
}

static inline struct buffer_pool *
pool_pop()
{
	if (pool == pool->next) /* empty pool */
		return NULL;

	struct buffer_pool *item;
	item = list_entry(pool->next,
			  struct buffer_pool,
			  list);
	list_del(&item->list);

	return item;
}

static gboolean
deinit_codec(GstSyslinkVDec *self)
{
	gboolean ret = TRUE;

	if (!self->codec)
		return TRUE;

	if (self->input)
		dce_buffer_free(self->ce,
				self->input);

	if (self->inbufs)
		dce_buffer_free(self->ce,
				self->inbufs);

	pool_free(self);

	if (self->outbufs)
		dce_buffer_free(self->ce,
				self->outbufs);

	if (!dce_viddec_delete(self->ce,
			       self->codec))
		ret = FALSE;
	else
		self->codec = NULL;

	deallocate_params(self);

	return ret;
}

static inline gboolean
process_input(GstSyslinkVDec *self,
	      GstBuffer *buf)
{
	guint offset = 0;

	if (self->codec_data) {
		GstBuffer *b = self->codec_data;
		self->codec_data = NULL;

		offset = GST_BUFFER_SIZE(b);

		memcpy(self->input,
		       GST_BUFFER_DATA(b),
		       offset);

		gst_buffer_unref(b);
	}

	guint size = offset + GST_BUFFER_SIZE(buf);

	if (size == 0)
		return FALSE;

	memcpy(self->input + offset,
	       GST_BUFFER_DATA(buf),
	       GST_BUFFER_SIZE(buf));

	struct viddec_in_args *inargs;
	inargs = (struct viddec_in_args *) self->inargs;
	inargs->num_bytes = size;

	struct buf_desc *inbufs;
	inbufs = (struct buf_desc *) self->inbufs;
	inbufs->descs[0].buf_size.bytes = size;

	return TRUE;
}

static inline guint8 *
out_buffer_get_data(GstSyslinkVDec *self)
{
	void *data;
	struct buffer_pool *item;

	item = pool_pop();
	if (item) {
		data = item->data;
		free(item);
		GST_DEBUG_OBJECT(self, "reusing tile buffer %p", data);
		return (guint8 *) data;
	}

	if (!dce_tile_alloc(self->ce,
			    self->padded_width,
			    self->padded_height,
			    &data))
		return NULL;

	GST_DEBUG_OBJECT(self, "new tile buffer %p", data);

	return (guint8 *) data;
}

static void
out_buffer_free(gpointer data)
{
	struct buffer_pool *item;

	item = calloc(1, sizeof(*item));
	item->data = data;
	pool_push(item);

	GST_DEBUG("tile buffer queued %p", data);
}

static inline int16_t
get_mem_type(uintptr_t pa)
{
	if (0x60000000 <= pa && pa < 0x68000000)
		return 1; /* TILED8 */

	if (0x68000000 <= pa && pa < 0x70000000)
		return 2; /* TILED16 */

	if (0x70000000 <= pa && pa < 0x78000000)
		return 3; /* TILED32 */

	if (0x78000000 <= pa && pa < 0x80000000)
		return 0; /* RAW */

	return -1;
}

static inline bool
is_tile(GstSyslinkVDec *self,
	GstBuffer *buf)
{
	guint8 *y_va = GST_BUFFER_DATA(buf);
	guint8 *uv_va = y_va + 4096 * self->padded_height;

	uintptr_t y_pa = dce_tile_get_pa(self->ce, y_va);
	uintptr_t uv_pa = dce_tile_get_pa(self->ce, uv_va);

	int16_t y_type = get_mem_type(y_pa);
	int16_t uv_type = get_mem_type(uv_pa);

	if (y_type == -1 || uv_type == -1)
		return false;

	return true;
}

static GstBuffer *
out_buffer_new(GstSyslinkVDec *self)
{
	size_t size = GST_ROUND_UP_2(4096 * self->padded_height * 3) / 2;
	GstCaps *caps = GST_PAD_CAPS(self->srcpad);

	GstBuffer *buf;

	{
		GstFlowReturn ret;
		ret = gst_pad_alloc_buffer_and_set_caps (self->srcpad,
							 0,
							 size,
							 caps,
							 &buf);

		if (ret == GST_FLOW_OK) {
			if (is_tile(self, buf))
				return buf;
			else
				gst_buffer_unref(buf);
		}
	}

	guint8 *data = out_buffer_get_data(self);
	if (!data)
		return NULL;

	buf = gst_buffer_new();
	GST_BUFFER_MALLOCDATA(buf) = data;
	GST_BUFFER_DATA(buf) = data;
	GST_BUFFER_SIZE(buf) = size;
	GST_BUFFER_FREE_FUNC(buf) = out_buffer_free;

	gst_buffer_set_caps(buf, caps);

	GST_DEBUG_OBJECT(self, "gst buffer created %p / %p", buf, data);

	return buf;
}

static inline gboolean
allocate_outbufs(GstSyslinkVDec *self,
		 int16_t y_type,
		 int16_t uv_type)
{
	struct single_buf_desc_2 descs[] = {
		{
			.mem_type = y_type,
			.buf_size.tile_mem = {
				.width = self->padded_width,
				.height = self->padded_height,
			},
		},
		{
			.mem_type = uv_type,
			.buf_size.tile_mem = {
				.width = self->padded_width,
				.height = self->padded_height / 2,
			},
		},

	};

	if (!viddec_set_buffer_desc(self->ce,
				    2,
				    descs,
				    &self->outbufs))
		return FALSE;

	return TRUE;
}

static inline gboolean
process_output(GstSyslinkVDec *self,
	       GstBuffer *buf)
{
	guint8 *y_va = GST_BUFFER_DATA(buf);
	guint8 *uv_va = y_va + 4096 * self->padded_height;

	uintptr_t y_pa = dce_tile_get_pa(self->ce, y_va);
	uintptr_t uv_pa = dce_tile_get_pa(self->ce, uv_va);

	int16_t y_type = get_mem_type(y_pa);
	int16_t uv_type = get_mem_type(uv_pa);

	if (y_type == -1 || uv_type == -1)
		return FALSE;

	if (!self->outbufs) {
		if (!allocate_outbufs(self,
				      y_type,
				      uv_type))
			return FALSE; /* @TODO verify a secure state */
	}

	struct buf_desc *outbufs;
	outbufs = (struct buf_desc *) self->outbufs;
	outbufs->descs[0].buf = (int8_t *) y_pa;
	outbufs->descs[1].buf = (int8_t *) uv_pa;

	return TRUE;
}

static inline void
send_crop_event(GstSyslinkVDec *self)
{
	struct viddec_out_args *outargs;
	outargs = (struct viddec_out_args *) self->outargs;

	struct rect *r = &outargs->display_bufs.buf_desc[0].active_frame_region;
	GST_DEBUG_OBJECT (self, "setting crop to %d, %d, %d, %d",
			  r->top_left.x,
			  r->top_left.y,
			  r->bottom_right.x,
			  r->bottom_right.y);

	GstEvent *event;
	event = gst_event_new_crop(r->top_left.y,
				   r->top_left.x,
				   r->bottom_right.x - r->top_left.x,
				   r->bottom_right.y - r->top_left.y);
	gst_pad_push_event(self->srcpad, event);
}

static gboolean
process_codec(GstSyslinkVDec *self, guint8 flags)
{
	GstBuffer *buf;
	gboolean ret = TRUE;

	struct viddec_out_args *outargs;
	outargs = (struct viddec_out_args *) self->outargs;

	if (!dce_viddec_process(self->ce,
				self->codec,
				self->inbufs,
				self->outbufs,
				self->inargs,
				self->outargs)) {

		GST_WARNING_OBJECT(self, "arguments error = 0x%x",
				   outargs->extended_error);

		if (dce_viddec_control(self->ce,
				       self->codec,
				       0, /* get status */
				       self->dynparams,
				       self->status))
			GST_WARNING_OBJECT(self, "status error = %08x",
					   viddec_status_get_error(self->status));

		gboolean fatal = (outargs->extended_error >> 15) & 0x1;
		gboolean flush = flags & FLUSH_BUFFERS;
		ret = !(fatal || flush);
	}

	for (int i = 0; outargs->output_id[i] != 0; i++) {
		if (self->send_crop_event) {
			send_crop_event(self);
			self->send_crop_event = FALSE;
		}

		if ((flags & PUSH_BUFFERS) == 0)
			break;

		buf = GST_BUFFER(outargs->output_id[i]);
		gst_buffer_ref(buf);

		GST_DEBUG_OBJECT(self, "pushing buffer: %"
				 GST_TIME_FORMAT
				 " - %p / %p",
				 GST_TIME_ARGS(GST_BUFFER_TIMESTAMP(buf)),
				 buf, GST_BUFFER_DATA(buf));

		GstFlowReturn flow;
		flow = gst_pad_push(self->srcpad, buf);

		if (G_UNLIKELY(flow != GST_FLOW_OK)) {
			GST_INFO_OBJECT(self, "pad push failed: %s",
					gst_flow_get_name(flow));
			ret = FALSE;
		}
	}

	for (int i = 0; outargs->free_buf_id[i] != 0; i++) {
		buf = GST_BUFFER(outargs->free_buf_id[i]);
		gst_buffer_unref(buf);
	}

	return ret;
}

static gboolean
flush_codec(GstSyslinkVDec *self, gboolean push_buffers)
{
	gboolean ret = TRUE;
	/* note: flush is synchronized against _chain() to avoid calling
	 * the codec from multiple threads
	 */
	GST_PAD_STREAM_LOCK (self->sinkpad);

	if (!dce_viddec_control(self->ce,
				self->codec,
				4, /* flush */
				self->dynparams,
				self->status)) {
		GST_ERROR_OBJECT(self, "flush failed");
		ret = FALSE;
		goto out;
	}

	struct buf_desc *inbufs;
	inbufs = (struct buf_desc *) self->inbufs;
	inbufs->descs[0].buf_size.bytes = 0;

	struct viddec_in_args *inargs;
	inargs = (struct viddec_in_args *) self->inargs;
	inargs->num_bytes = 0;
	inargs->input_id = 0;

	guint8 flags = FLUSH_BUFFERS;
	if (push_buffers)
		flags |= PUSH_BUFFERS;

	while (process_codec(self, flags));

out:
	GST_PAD_STREAM_UNLOCK (self->sinkpad);
	return ret;
}

static gboolean
sink_event(GstPad *pad,
	   GstEvent *event)
{
	GstSyslinkVDec *self;
	self = GST_SYSLINK_VDEC(GST_OBJECT_PARENT(pad));

	GST_INFO_OBJECT(self, "event = %s", GST_EVENT_TYPE_NAME(event));

	switch (GST_EVENT_TYPE (event)) {
	case GST_EVENT_EOS:
		if (!flush_codec(self, TRUE))
			return FALSE;
		break;
	case GST_EVENT_FLUSH_STOP:
		if (!flush_codec(self, FALSE))
			return FALSE;
		break;
	default:
		break;
	}

	return gst_pad_push_event (self->srcpad, event);
}

static GstFlowReturn
pad_chain(GstPad *pad,
	  GstBuffer *buf)
{
	GstSyslinkVDec *self;
	GstFlowReturn ret = GST_FLOW_OK;

	self = GST_SYSLINK_VDEC(GST_OBJECT_PARENT(pad));

	if (G_UNLIKELY(!self->codec)) {
		if (!init_codec(self)) {
			GST_ERROR_OBJECT(self, "could not create codec");
			ret = GST_FLOW_ERROR;
			goto leave;
		}
	}

	/* copy the first buffer with the codec_data */
	if (!process_input(self, buf)) {
		GST_DEBUG_OBJECT (self, "no input, skipping process");
		goto leave;
	}

	GstBuffer *outbuf = out_buffer_new(self);
	GST_BUFFER_TIMESTAMP(outbuf) = GST_BUFFER_TIMESTAMP(buf);
	GST_BUFFER_DURATION(outbuf) = GST_BUFFER_DURATION(buf);

	if (!process_output(self, outbuf)) {
		GST_ERROR_OBJECT (self, "could not process output buffer");
		ret = GST_FLOW_ERROR;
		goto leave;
	}

	struct viddec_in_args *inargs;
	inargs = (struct viddec_in_args *) self->inargs;
	inargs->input_id = (int32_t) outbuf;

	if (!process_codec(self, PUSH_BUFFERS)) {
		ret = GST_FLOW_ERROR;
		goto leave;
	}

leave:
	gst_buffer_unref(buf);
	return ret;
}


static inline GstCaps *
generate_sink_template(void)
{
	GstCaps *caps;
	GstStructure *struc;

	caps = gst_caps_new_empty();
	struc = gst_structure_new("video/x-h264",
				  "alignment", G_TYPE_STRING, "au",
				  NULL);
	gst_caps_append_structure(caps, struc);

	return caps;
}

static inline GstCaps *
generate_src_template(void)
{
	GstCaps *caps;
	GstStructure *struc;

	caps = gst_caps_new_empty();
	struc = gst_structure_new("video/x-raw-yuv-strided",
		"format", GST_TYPE_FOURCC, GST_MAKE_FOURCC('N', 'V', '1', '2'),
		"rowstride", GST_TYPE_INT_RANGE, 0, G_MAXINT,
		NULL);
	gst_caps_append_structure(caps, struc);

	return caps;
}

static gboolean
syslink_open(GstSyslinkVDec *self)
{
	int ipc_handle;

	self->ipc_handle = \
		ipc_handle = s_ipc_open();
	if (ipc_handle == -1)
		 return FALSE;

	if (!s_ipc_setup(ipc_handle))
		goto bail;

	self->ce = dce_new(ipc_handle);
	if (!self->ce)
		goto destroy;

	if (!dce_init(self->ce))
		goto dce_bail;

	if (!dce_open(self->ce, "ivahd_vidsvr"))
		goto deinit;

	return TRUE;

deinit:
	dce_deinit(self->ce);

dce_bail:
	dce_free(self->ce);

destroy:
	s_ipc_destroy(ipc_handle);

bail:
	close(ipc_handle);

	return FALSE;
}

static void
syslink_close(GstSyslinkVDec *self)
{
	if (self->ce) {
		dce_close(self->ce);
		dce_deinit(self->ce);
		dce_free(self->ce);
		self->ce = NULL;
	}

	if (self->ipc_handle >= 0) {
		s_ipc_destroy(self->ipc_handle);
		close(self->ipc_handle);
	}
}

static GstStateChangeReturn
change_state(GstElement *element,
	     GstStateChange transition)
{
	GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
	GstSyslinkVDec *self;

	self = GST_SYSLINK_VDEC(element);

	GST_INFO_OBJECT(self, "%s -> %s",
		gst_element_state_get_name(GST_STATE_TRANSITION_CURRENT(transition)),
		gst_element_state_get_name(GST_STATE_TRANSITION_NEXT(transition)));

	switch (transition) {
	case GST_STATE_CHANGE_NULL_TO_READY:
		if (!syslink_open(self)) {
			GST_ERROR_OBJECT(self, "could not open");
			return GST_STATE_CHANGE_FAILURE;
		}
		break;

	default:
		break;
	}

	ret = GST_ELEMENT_CLASS(parent_class)->change_state(element, transition);
	if (ret == GST_STATE_CHANGE_FAILURE)
		return ret;

	switch (transition) {
	case GST_STATE_CHANGE_PAUSED_TO_READY:
		if (!flush_codec(self, FALSE))
			GST_WARNING_OBJECT(self, "flush failed");

		if (!deinit_codec(self)) {
			GST_ERROR_OBJECT(self, "codec delete failed");
			return GST_STATE_CHANGE_FAILURE;
		}
		gst_caps_replace(&self->tmp_caps, NULL);
		break;

	case GST_STATE_CHANGE_READY_TO_NULL:
		syslink_close(self);
		break;

	default:
		break;
	}

	return ret;
}

static void
instance_init(GTypeInstance *instance,
	      void *g_class)
{
	GstElementClass *gst_class = g_class;
	GstSyslinkVDec *self = GST_SYSLINK_VDEC(instance);

	GstPadTemplate *template;
	template = gst_element_class_get_pad_template(gst_class, "sink");
	self->sinkpad = gst_pad_new_from_template(template, "sink");

	gst_pad_set_setcaps_function(self->sinkpad, sink_setcaps);
	gst_pad_set_chain_function(self->sinkpad, pad_chain);
	gst_pad_set_event_function(self->sinkpad, sink_event);

	template = gst_element_class_get_pad_template(gst_class, "src");
	self->srcpad = gst_pad_new_from_template(template, "src");

	gst_pad_use_fixed_caps(self->srcpad);

	gst_element_add_pad(GST_ELEMENT(self), self->sinkpad);
	gst_element_add_pad(GST_ELEMENT(self), self->srcpad);

	pool = &self->pool;
	INIT_LIST_HEAD(pool);
}

static void
base_init(gpointer g_class)
{
	GstElementClass *gst_class;
	GstPadTemplate *template;

	gst_class = GST_ELEMENT_CLASS(g_class);

	gst_element_class_set_details_simple(gst_class,
					     "SysLink video decoder",
					     "Codec/Decoder/Video",
					     "Decodes video with TI's Ducati",
					     "Víctor Jáquez");

	template = gst_pad_template_new("src", GST_PAD_SRC,
					GST_PAD_ALWAYS,
					generate_src_template());
	gst_element_class_add_pad_template(gst_class, template);
	gst_object_unref(template);

	template = gst_pad_template_new("sink", GST_PAD_SINK,
					GST_PAD_ALWAYS,
					generate_sink_template());
	gst_element_class_add_pad_template(gst_class, template);
	gst_object_unref(template);
}

static void
class_init(gpointer klass,
	   gpointer data)
{
	GstElementClass *gst_class;

	parent_class = g_type_class_peek_parent(klass);

	gst_class = GST_ELEMENT_CLASS(klass);
	gst_class->change_state = change_state;
}

GType
gst_syslink_vdec_get_type(void)
{
	static GType type;

	if (G_UNLIKELY(type == 0)) {
		GTypeInfo info = {
			.class_size = sizeof(GstSyslinkVDecClass),
			.class_init = class_init,
			.base_init = base_init,
			.instance_size = sizeof(GstSyslinkVDec),
			.instance_init = instance_init,
		};

		type = g_type_register_static(GST_TYPE_ELEMENT,
					      "GstSyslinkVDec",
					      &info,
					      0);
	}

	return type;
}
