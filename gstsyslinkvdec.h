/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#ifndef GST_SYSLINK_VDEC_H
#define GST_SYSLINK_VDEC_H

#include <gst/gst.h>

#include "dce.h"
#include "list.h"

G_BEGIN_DECLS

#define GST_SYSLINK_VDEC(obj) (GstSyslinkVDec *)(obj)
#define GST_SYSLINK_VDEC_TYPE (gst_syslink_vdec_get_type())
#define GST_SYSLINK_VDEC_CLASS(obj) (GstSyslinkVDecClass *)(obj)

enum {
	GSTSYSLINK_H264DEC,
};

typedef struct _GstSyslinkVDec GstSyslinkVDec;
typedef struct _GstSyslinkVDecClass GstSyslinkVDecClass;

struct _GstSyslinkVDec {
	GstElement element;
	GstPad *sinkpad, *srcpad;

	uint alg;
	int ipc_handle;
	struct codec_engine *ce;
	void *codec;
	void *params, *dynparams, *status;
	void *inbufs, *outbufs;
	void *inargs, *outargs;

	/* input buffer, allocated when codec is created: */
	void *input;

	/* input (unpadded) size of video: */
	int width, height;

	/* output (padded) size including any codec padding: */
	int padded_width, padded_height;

	GstBuffer *codec_data;
	GstCaps *tmp_caps;

	struct list_head pool;

	gboolean send_crop_event;
};

struct _GstSyslinkVDecClass {
	GstElementClass parent_class;
};

GType gst_syslink_vdec_get_type(void);

G_END_DECLS

#endif /* GST_SYSLINK_VDEC_H */
