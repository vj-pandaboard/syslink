/*
 * Copyright (C) 2011 Igalia S.L.
 *
 * Author: Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This file may be used under the terms of the GNU Lesser General Public
 * License version 2.1, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

/* Direct Codec Engine */

#include <string.h>
#include <unistd.h> /* getpid */
#include <stdlib.h> /* malloc */
#include <assert.h> /* assert */

#include "dce.h"
#include "util.h"
#include "tiler.h"
#include "list.h"
#include "log.h"

struct buffer_list {
	uintptr_t pa;
	struct tiler_buffer_info *buffer;
	struct list_head list;
};

struct priv {
	void *handle;
	struct list_head cache;
};

#define PRIV(ce) ((struct priv *) ce->priv)

static uint32_t idx_open;
static uint32_t idx_close;
static uint32_t idx_vdec_create;
static uint32_t idx_vdec_control;
static uint32_t idx_vdec_process;
static uint32_t idx_vdec_delete;

struct {
	const char *name;
	uint32_t *idx;
} remote_functions[] = {
	{ "Engine_open",     &idx_open,         },
	{ "Engine_close",    &idx_close,        },
	{ "VIDDEC3_create",  &idx_vdec_create,  },
	{ "VIDDEC3_control", &idx_vdec_control, },
	{ "VIDDEC3_process", &idx_vdec_process, },
	{ "VIDDEC3_delete",  &idx_vdec_delete,  },
};

struct codec_engine *
dce_new(int ipc_handle)
{
	struct rcm_client *rcm;
	rcm = rcm_client_new(ipc_handle);
	if (!rcm)
		return NULL;

	int tiler_handle = tiler_open();
	if (tiler_handle == -1)
		goto bail;

	struct codec_engine *ce;
	ce = calloc(1, sizeof(*ce));
	ce->rcm = rcm;
	ce->tiler_handle = tiler_handle;

	return ce;

bail:
	if (rcm)
		rcm_client_free(rcm);

	return NULL;
}

void
dce_free(struct codec_engine *ce)
{
	close(ce->tiler_handle);
	rcm_client_free(ce->rcm);
	free(ce);
}

static inline bool
get_functions(struct codec_engine *ce)
{
	for (unsigned int i = 0; i < ARRAY_SIZE(remote_functions); i++) {
		if (!rcm_client_get_symbol_index(ce->rcm,
						 remote_functions[i].name,
						 remote_functions[i].idx))
			return false;
	}

	return true;
}

bool
dce_init(struct codec_engine *ce)
{
	ce->priv = calloc(1, sizeof(struct priv));
	INIT_LIST_HEAD(&PRIV(ce)->cache);

	if (!rcm_client_init(ce->rcm, "dCE"))
		return false;

	if (!get_functions(ce))
		return false;

	return true;
}

static void
free_cache(struct codec_engine *ce)
{
	struct buffer_list *entry, *tmp;
	struct tiler_buffer_info *b;
	struct list_head *list;

	list = &PRIV(ce)->cache;
	list_for_each_entry_safe(entry,
				 tmp,
				 list,
				 list) {
		b = entry->buffer;
		{
			pr_warning("tile leak = %p / %p", b, b->blocks[0].ptr);
			tiler_free(ce->tiler_handle, b);
			free(b);
		}
		list_del(&entry->list);
		free(entry);
	}
}

bool
dce_deinit(struct codec_engine *ce)
{
	free_cache(ce);
	free(PRIV(ce));
	return rcm_client_deinit(ce->rcm);
}

struct dce_open_args_in {
	int pid;
	char name[25];
};

struct dce_open_args_out {
	int error;
	void *handle;
};

bool
dce_open(struct codec_engine *ce,
	 const char *name)
{
	bool ret = false;
	struct rcm_client_msg *msg = NULL;
	struct dce_open_args_in in = {
		.pid = getpid(),
	};

	strncpy(in.name, name, 24);

	if (!rcm_client_alloc(ce->rcm,
			      sizeof(in),
			      &msg))
		goto bail;

	memcpy(&msg->data, &in, sizeof(in));
	msg->func_idx = idx_open;

	if (!rcm_client_exec(ce->rcm, msg, &msg))
		goto bail;

	struct dce_open_args_out *out;
	out = (struct dce_open_args_out *) &msg->data;
	ret = out->error == 0;
	PRIV(ce)->handle = out->handle;

bail:
	if (msg)
		rcm_client_free_msg(ce->rcm, msg);

	return ret;
}

struct dce_close_args {
	int pid;
	void *handle;
};

bool
dce_close(struct codec_engine *ce)
{
	bool ret = false;
	struct rcm_client_msg *msg;
	struct dce_close_args args = {
		.pid = getpid(),
		.handle = PRIV(ce)->handle,
	};

	if (!rcm_client_alloc(ce->rcm,
			      sizeof(args),
			      &msg))
		goto bail;

	memcpy(&msg->data, &args, sizeof(args));
	msg->func_idx = idx_close;

	if (!rcm_client_exec(ce->rcm, msg, &msg))
		goto bail;

	ret = true;

bail:
	if (msg)
		rcm_client_free_msg(ce->rcm, msg);

	return ret;
}

static void
cache_add_buffer(struct list_head *head,
		 struct tiler_buffer_info *buffer)
{
	struct buffer_list *tmp;
	tmp = malloc(sizeof(struct buffer_list));
	tmp->pa = 0;
	tmp->buffer = buffer;
	list_add(&tmp->list,
		 head);
}

/* searches a buffer and if found, it is deleted from the list and
 * freed */
static struct tiler_buffer_info *
cache_get_buffer(struct list_head *head,
		 void *ptr)
{
	struct buffer_list *entry, *tmp;
	struct tiler_buffer_info *buffer;
	void *base;

	list_for_each_entry_safe(entry,
				 tmp,
				 head,
				 list) {
		buffer = entry->buffer;
		base = buffer->blocks[0].ptr;
		if (base == ptr) {
			list_del(&entry->list);
			free(entry);
			return buffer;
		}
	}

	return NULL;
}

/* searches a buffer and if found, returns the list's entry */
static struct buffer_list *
cache_search_entry(struct list_head *head,
		   void *iova)
{
	struct buffer_list *entry, *tmp;
	void *base;

	list_for_each_entry_safe(entry,
				 tmp,
				 head,
				 list) {
		base = entry->buffer->blocks[0].ptr;
		if (base == iova)
			return entry;
	}

	return NULL;
}

static inline bool
alloc(struct codec_engine *ce,
      struct tiler_buffer_info *buffer,
      void **ptr)
{
	if (!tiler_alloc(ce->tiler_handle, buffer))
		return false;

	*ptr = buffer->blocks[0].ptr;
	cache_add_buffer(&PRIV(ce)->cache, buffer);

	return true;
}

bool
dce_buffer_alloc(struct codec_engine *ce,
		 size_t size,
		 void **ptr)
{
	struct tiler_block_info block = {
		.fmt = TILFMT_PAGE,
		.len = size,
	};

	struct tiler_buffer_info *buffer;
	buffer = calloc(1, sizeof(struct tiler_buffer_info));
	buffer->num_blocks = 1;
	memcpy(&buffer->blocks[0],
	       &block,
	       sizeof(block));

	if (!alloc(ce, buffer, ptr))
		goto bail;

	memset(*ptr, 0, size);

	return true;

bail:
	free(buffer);
	return false;
}

struct area {
	int16_t w, h;
};

static inline int32_t
pack_area(int16_t width,
	  int16_t height)
{
	int32_t ret;
	struct area a = { width, height };
	memcpy(&ret, &a, sizeof(a));
	return ret;
}

bool
dce_tile_alloc(struct codec_engine *ce,
	       int16_t width,
	       int16_t height,
	       void **ptr)
{
	/* if the user only provides the width, we assume that a
	 * linear buffer is required */
	if (height == 0)
		return dce_buffer_alloc(ce, width, ptr);

	struct tiler_block_info block[2] = {
		{
			.fmt = TILFMT_8BIT,
			.len = pack_area(width, ALIGN(height, 1)),
			.stride = 4096,
		},
		{
			.fmt = TILFMT_16BIT,
			.len = pack_area(width, ALIGN(height, 1) / 2),
			.stride = 4096,
		},
	};

	struct tiler_buffer_info *buffer;
	buffer = calloc(1, sizeof(struct tiler_buffer_info));
	buffer->num_blocks = 2;
	memcpy(&buffer->blocks,
	       &block,
	       sizeof(block));

	if (!alloc(ce, buffer, ptr))
		goto bail;

	return true;

bail:
	free(buffer);
	return false;
}

bool
dce_buffer_free(struct codec_engine *ce,
		void *ptr)
{
	struct tiler_buffer_info *buffer;
	buffer = cache_get_buffer(&PRIV(ce)->cache, ptr);

	if (!buffer) {
		pr_warning("buffer not found = %p", ptr);
		return false;
	}

	bool ret;
	ret = tiler_free(ce->tiler_handle, buffer);
	free(buffer);

	return ret;
}

uintptr_t
dce_tile_get_pa(struct codec_engine *ce,
		void *iova)
{
	struct buffer_list *e;
	e = cache_search_entry(&PRIV(ce)->cache, iova);

	if (e && e->pa != 0)
		return e->pa;

	uintptr_t ret;
	if (!tiler_get_pa(ce->tiler_handle,
			  iova,
			  &ret))
		return 0;

	if (e)
		e->pa = ret;

	return ret;
}

void *
dce_tile_get_iova(struct codec_engine *ce,
		  uint32_t pa)
{
	struct buffer_list *entry, *tmp;
	struct list_head *head;

	head = &PRIV(ce)->cache;
	list_for_each_entry_safe(entry,
				 tmp,
				 head,
				 list) {
		if (entry->pa != 0
		    && entry->pa == pa)
			return entry->buffer->blocks[0].ptr;
	}

	return NULL;
}

struct viddec_create_args_in {
	int pid;
	void *handle;
	char name[25];
	uintptr_t params;
};

bool
dce_viddec_create(struct codec_engine *ce,
		  const char *name,
		  void *params,
		  void **codec)
{
	bool ret = false;
	struct viddec_create_args_in args = {
		.pid = getpid(),
		.handle = PRIV(ce)->handle,
		.params = dce_tile_get_pa(ce, params),
	};

	strncpy(args.name, name, 24);

	struct rcm_client_msg *msg = NULL;
	if (!rcm_client_alloc(ce->rcm,
			      sizeof(args),
			      &msg))
		goto bail;

	memcpy(&msg->data, &args, sizeof(args));
	msg->func_idx = idx_vdec_create;

	if (!rcm_client_exec(ce->rcm, msg, &msg))
		goto bail;

	*codec = (void *) msg->data[0];

	ret = *codec != NULL;

bail:
	if (msg)
		rcm_client_free_msg(ce->rcm, msg);

	return ret;
}

struct viddec_delete_args_in {
	int pid;
	uintptr_t codec;
};

bool
dce_viddec_delete(struct codec_engine *ce,
		  void *codec)
{
	bool ret = false;
	struct viddec_delete_args_in args = {
		.pid = getpid(),
		.codec = (uintptr_t) codec,
	};

	struct rcm_client_msg *msg = NULL;
	if (!rcm_client_alloc(ce->rcm,
			      sizeof(args),
			      &msg))
		goto bail;

	memcpy(&msg->data, &args, sizeof(args));
	msg->func_idx = idx_vdec_delete;

	if (!rcm_client_exec(ce->rcm, msg, &msg))
		goto bail;

	ret = true;

bail:
	if (msg)
		rcm_client_free_msg(ce->rcm, msg);

	return ret;
}

struct viddec_control_args_in {
	int pid;
	intptr_t codec;
        unsigned int id;
        uint32_t params;
        uint32_t status;
};

bool
dce_viddec_control(struct codec_engine *ce,
		   void *codec,
		   unsigned int id,
		   void *params,
		   void *status)
{
	bool ret = false;
	struct viddec_control_args_in args = {
		.pid = getpid(),
		.codec = (uintptr_t) codec,
		.id = id,
		.params = dce_tile_get_pa(ce, params),
		.status = dce_tile_get_pa(ce, status),
	};

	struct rcm_client_msg *msg = NULL;
	if (!rcm_client_alloc(ce->rcm,
			      sizeof(args),
			      &msg))
		goto bail;

	memcpy(&msg->data, &args, sizeof(args));
	msg->func_idx = idx_vdec_control;

	if (!rcm_client_exec(ce->rcm, msg, &msg))
		goto bail;

	ret = msg->data[0] == 0;

bail:
	if (msg)
		rcm_client_free_msg(ce->rcm, msg);

	return ret;
}

struct viddec_process_args_in {
	int pid;
	uintptr_t codec;
	uint32_t inbufs;
	uint32_t outbufs;
	uint32_t inargs;
	uint32_t outargs;
};

bool
dce_viddec_process(struct codec_engine *ce,
		   void *codec,
		   void *inbufs,
		   void *outbufs,
		   void *inargs,
		   void *outargs)
{
	bool ret = false;
	struct viddec_process_args_in args = {
		.pid = getpid(),
		.codec = (uintptr_t) codec,
		.inbufs = dce_tile_get_pa(ce, inbufs),
		.outbufs = dce_tile_get_pa(ce, outbufs),
		.inargs = dce_tile_get_pa(ce, inargs),
		.outargs = dce_tile_get_pa(ce, outargs),
	};

	struct rcm_client_msg *msg = NULL;
	if (!rcm_client_alloc(ce->rcm,
			      sizeof(args),
			      &msg))
		goto bail;

	memcpy(&msg->data, &args, sizeof(args));
	msg->func_idx = idx_vdec_process;

	if (!rcm_client_exec(ce->rcm, msg, &msg))
		goto bail;

	ret = msg->data[0] == 0;

bail:
	if (msg)
		rcm_client_free_msg(ce->rcm, msg);

	return ret;
}
